-- absolute_maximum_calc
CREATE OR REPLACE FUNCTION absolute_maximum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'absolute_maximum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- absolute_maximum
CREATE OR REPLACE AGGREGATE absolute_maximum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=absolute_maximum_calc
);

-- absolute_sum_of_changes_calc

CREATE OR REPLACE FUNCTION absolute_sum_of_changes_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_absolute_sum_of_changes_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION absolute_sum_of_changes_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'absolute_sum_of_changes_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- absolute_sum_of_changes
CREATE OR REPLACE AGGREGATE absolute_sum_of_changes(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=absolute_sum_of_changes_calc
);

CREATE OR REPLACE AGGREGATE absolute_sum_of_changes(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=absolute_sum_of_changes_calc
);

-- agg_autocorrelation_calc

CREATE OR REPLACE FUNCTION agg_autocorrelation_calc(state ts_agg_autocorrelation_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_agg_autocorrelation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION agg_autocorrelation_calc(state agg_autocorrelation_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'agg_autocorrelation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- agg_autocorrelation
CREATE OR REPLACE AGGREGATE agg_autocorrelation(REAL, TIMESTAMP, TEXT[], BIGINT[])
(
     SFUNC = ts_agg_autocorrelation_agg,
     STYPE = ts_agg_autocorrelation_type,
     COMBINEFUNC = combine_ts_agg_autocorrelation,
     PARALLEL = SAFE,
     FINALFUNC=agg_autocorrelation_calc
);

CREATE OR REPLACE AGGREGATE agg_autocorrelation(REAL, TEXT[], BIGINT[])
(
     SFUNC = agg_autocorrelation_agg,
     STYPE = agg_autocorrelation_type,
     COMBINEFUNC = combine_agg_autocorrelation,
     PARALLEL = SAFE,
     FINALFUNC=agg_autocorrelation_calc
);

-- approximate_entropy_calc

CREATE OR REPLACE FUNCTION approximate_entropy_calc(state ts_approx_entropy_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_approximate_entropy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION approximate_entropy_calc(state approx_entropy_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'approximate_entropy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- approximate_entropy
CREATE OR REPLACE AGGREGATE approximate_entropy(REAL, TIMESTAMP, BIGINT, REAL)
(
     SFUNC = ts_approx_entropy_agg,
     STYPE = ts_approx_entropy_type,
     COMBINEFUNC = combine_ts_approx_entropy,
     PARALLEL = SAFE,
     FINALFUNC=approximate_entropy_calc
);

CREATE OR REPLACE AGGREGATE approximate_entropy(REAL, BIGINT, REAL)
(
     SFUNC = approx_entropy_agg,
     STYPE = approx_entropy_type,
     COMBINEFUNC = combine_approx_entropy,
     PARALLEL = SAFE,
     FINALFUNC=approximate_entropy_calc
);

-- augmented_dickey_fuller_calc

CREATE OR REPLACE FUNCTION augmented_dickey_fuller_calc(state ts_agg_dickey_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'augmented_dickey_fuller_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION augmented_dickey_fuller_calc(state agg_dickey_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'augmented_dickey_fuller_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- augmented_dickey_fuller
CREATE OR REPLACE AGGREGATE augmented_dickey_fuller(REAL, TIMESTAMP, TEXT[], TEXT[])
(
     SFUNC = ts_agg_dickey_agg,
     STYPE = ts_agg_dickey_type,
     COMBINEFUNC = combine_ts_agg_dickey,
     PARALLEL = SAFE,
     FINALFUNC=augmented_dickey_fuller_calc
);

CREATE OR REPLACE AGGREGATE augmented_dickey_fuller(REAL, TEXT[], TEXT[])
(
     SFUNC = agg_dickey_agg,
     STYPE = agg_dickey_type,
     COMBINEFUNC = combine_agg_dickey,
     PARALLEL = SAFE,
     FINALFUNC=augmented_dickey_fuller_calc
);

-- autocorrelation_calc

CREATE OR REPLACE FUNCTION autocorrelation_calc(state single_date_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_autocorrelation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION autocorrelation_calc(state single_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'autocorrelation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- autocorrelation
CREATE OR REPLACE AGGREGATE autocorrelation(REAL, TIMESTAMP, BIGINT)
(
     SFUNC = single_date_1_p_agg,
     STYPE = single_date_1_p_type,
     COMBINEFUNC = combine_single_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=autocorrelation_calc
);

CREATE OR REPLACE AGGREGATE autocorrelation(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=autocorrelation_calc
);

-- binned_entropy_calc

CREATE OR REPLACE FUNCTION binned_entropy_calc(state single_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'binned_entropy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- binned_entropy
CREATE OR REPLACE AGGREGATE binned_entropy(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=binned_entropy_calc
);

-- c3_calc

CREATE OR REPLACE FUNCTION c3_calc(state single_date_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_c3_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION c3_calc(state single_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'c3_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- c3
CREATE OR REPLACE AGGREGATE c3(REAL, TIMESTAMP, BIGINT)
(
     SFUNC = single_date_1_p_agg,
     STYPE = single_date_1_p_type,
     COMBINEFUNC = combine_single_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=c3_calc
);

CREATE OR REPLACE AGGREGATE c3(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=c3_calc
);

-- cid_ce_calc

CREATE OR REPLACE FUNCTION cid_ce_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_cid_ce_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION cid_ce_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'cid_ce_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- cid_ce
CREATE OR REPLACE AGGREGATE cid_ce(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=cid_ce_calc
);

CREATE OR REPLACE AGGREGATE cid_ce(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=cid_ce_calc
);

-- count_above_calc
CREATE OR REPLACE FUNCTION count_above_calc(state single_float_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'count_above_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- count_above
CREATE OR REPLACE AGGREGATE count_above(REAL, REAL)
(
     SFUNC = single_float_1_p_agg,
     STYPE = single_float_1_p_type,
     COMBINEFUNC = combine_single_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=count_above_calc
);

-- count_above_mean_calc

CREATE OR REPLACE FUNCTION count_above_mean_calc(state REAL[]) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'count_above_mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- count_above_mean
CREATE OR REPLACE AGGREGATE count_above_mean(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=count_above_mean_calc
);

-- count_below_calc
CREATE OR REPLACE FUNCTION count_below_calc(state single_float_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'count_below_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- count_below
CREATE OR REPLACE AGGREGATE count_below(REAL, REAL)
(
     SFUNC = single_float_1_p_agg,
     STYPE = single_float_1_p_type,
     COMBINEFUNC = combine_single_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=count_below_calc
);

-- count_below_mean_calc

CREATE OR REPLACE FUNCTION count_below_mean_calc(state REAL[]) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'count_below_mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- count_below_mean
CREATE OR REPLACE AGGREGATE count_below_mean(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=count_below_mean_calc
);

-- energy_calc

CREATE OR REPLACE FUNCTION energy_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_energy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION energy_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'energy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- energy
CREATE OR REPLACE AGGREGATE energy(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=energy_calc
);

CREATE OR REPLACE AGGREGATE energy(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=energy_calc
);

-- energy_ratio_by_chunks_calc

CREATE OR REPLACE FUNCTION energy_ratio_by_chunks_calc(state ts_energy_ratio_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_energy_ratio_by_chunks_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION energy_ratio_by_chunks_calc(state energy_ratio_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'energy_ratio_by_chunks_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- energy_ratio_by_chunks
CREATE OR REPLACE AGGREGATE energy_ratio_by_chunks(REAL, TIMESTAMP, BIGINT[], BIGINT[])
(
     SFUNC = ts_energy_ratio_agg,
     STYPE = ts_energy_ratio_type,
     COMBINEFUNC = combine_ts_energy_ratio,
     PARALLEL = SAFE,
     FINALFUNC=energy_ratio_by_chunks_calc
);

CREATE OR REPLACE AGGREGATE energy_ratio_by_chunks(REAL, BIGINT[], BIGINT[])
(
     SFUNC = energy_ratio_agg,
     STYPE = energy_ratio_type,
     COMBINEFUNC = combine_energy_ratio,
     PARALLEL = SAFE,
     FINALFUNC=energy_ratio_by_chunks_calc
);

-- fft_aggregated_calc

CREATE OR REPLACE FUNCTION fft_aggregated_calc(state ts_fft_aggregated_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_fft_aggregated_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION fft_aggregated_calc(state fft_aggregated_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'fft_aggregated_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- fft_aggregated
CREATE OR REPLACE AGGREGATE fft_aggregated(REAL, TIMESTAMP, TEXT[])
(
     SFUNC = ts_fft_aggregated_agg,
     STYPE = ts_fft_aggregated_type,
     COMBINEFUNC = combine_ts_fft_aggregated,
     PARALLEL = SAFE,
     FINALFUNC=fft_aggregated_calc
);

CREATE OR REPLACE AGGREGATE fft_aggregated(REAL, TEXT[])
(
     SFUNC = fft_aggregated_agg,
     STYPE = fft_aggregated_type,
     COMBINEFUNC = combine_fft_aggregated,
     PARALLEL = SAFE,
     FINALFUNC=fft_aggregated_calc
);

-- fft_coefficient_calc

CREATE OR REPLACE FUNCTION fft_coefficient_calc(state ts_fft_coeff_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_fft_coefficient_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION fft_coefficient_calc(state fft_coeff_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'fft_coefficient_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- fft_coefficient
CREATE OR REPLACE AGGREGATE fft_coefficient(REAL, TIMESTAMP, BIGINT[], TEXT[])
(
     SFUNC = ts_fft_coeff_agg,
     STYPE = ts_fft_coeff_type,
     COMBINEFUNC = combine_ts_fft_coeff,
     PARALLEL = SAFE,
     FINALFUNC=fft_coefficient_calc
);

CREATE OR REPLACE AGGREGATE fft_coefficient(REAL, BIGINT[], TEXT[])
(
     SFUNC = fft_coeff_agg,
     STYPE = fft_coeff_type,
     COMBINEFUNC = combine_fft_coeff,
     PARALLEL = SAFE,
     FINALFUNC=fft_coefficient_calc
);

-- first_location_of_maximum_calc

CREATE OR REPLACE FUNCTION first_location_of_maximum_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_first_location_of_maximum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION first_location_of_maximum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'first_location_of_maximum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- first_location_of_maximum
CREATE OR REPLACE AGGREGATE first_location_of_maximum(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=first_location_of_maximum_calc
);

CREATE OR REPLACE AGGREGATE first_location_of_maximum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=first_location_of_maximum_calc
);

-- first_location_of_minimum_calc

CREATE OR REPLACE FUNCTION first_location_of_minimum_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_first_location_of_minimum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION first_location_of_minimum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'first_location_of_minimum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- first_location_of_minimum
CREATE OR REPLACE AGGREGATE first_location_of_minimum(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=first_location_of_minimum_calc
);

CREATE OR REPLACE AGGREGATE first_location_of_minimum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=first_location_of_minimum_calc
);

-- has_duplicate_calc

CREATE OR REPLACE FUNCTION has_duplicate_calc(state REAL[]) RETURNS BOOLEAN as '/usr/local/lib/libpgetu', 'has_duplicate_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- has_duplicate
CREATE OR REPLACE AGGREGATE has_duplicate(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=has_duplicate_calc
);

-- has_duplicate_max_calc

CREATE OR REPLACE FUNCTION has_duplicate_max_calc(state REAL[]) RETURNS BOOLEAN as '/usr/local/lib/libpgetu', 'has_duplicate_max_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- has_duplicate_max
CREATE OR REPLACE AGGREGATE has_duplicate_max(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=has_duplicate_max_calc
);

-- has_duplicate_min_calc

CREATE OR REPLACE FUNCTION has_duplicate_min_calc(state REAL[]) RETURNS BOOLEAN as '/usr/local/lib/libpgetu', 'has_duplicate_min_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- has_duplicate_min
CREATE OR REPLACE AGGREGATE has_duplicate_min(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=has_duplicate_min_calc
);

-- index_mass_quantile_calc

CREATE OR REPLACE FUNCTION index_mass_quantile_calc(state float_date_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_index_mass_quantile_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION index_mass_quantile_calc(state float_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'index_mass_quantile_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- index_mass_quantile
CREATE OR REPLACE AGGREGATE index_mass_quantile(REAL, TIMESTAMP, REAL[])
(
     SFUNC = float_date_1_p_agg,
     STYPE = float_date_1_p_type,
     COMBINEFUNC = combine_float_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=index_mass_quantile_calc
);

CREATE OR REPLACE AGGREGATE index_mass_quantile(REAL, REAL[])
(
     SFUNC = float_1_p_agg,
     STYPE = float_1_p_type,
     COMBINEFUNC = combine_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=index_mass_quantile_calc
);

-- kurtosis_calc

CREATE OR REPLACE FUNCTION kurtosis_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'kurtosis_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- kurtosis
CREATE OR REPLACE AGGREGATE kurtosis(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=kurtosis_calc
);

-- large_standard_deviation_calc

CREATE OR REPLACE FUNCTION large_standard_deviation_calc(state single_float_1_p_type) RETURNS BOOLEAN as '/usr/local/lib/libpgetu', 'large_standard_deviation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- large_standard_deviation
CREATE OR REPLACE AGGREGATE large_standard_deviation(REAL, REAL)
(
     SFUNC = single_float_1_p_agg,
     STYPE = single_float_1_p_type,
     COMBINEFUNC = combine_single_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=large_standard_deviation_calc
);

-- last_location_of_maximum_calc

CREATE OR REPLACE FUNCTION last_location_of_maximum_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_last_location_of_maximum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION last_location_of_maximum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'last_location_of_maximum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- last_location_of_maximum
CREATE OR REPLACE AGGREGATE last_location_of_maximum(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=last_location_of_maximum_calc
);

CREATE OR REPLACE AGGREGATE last_location_of_maximum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=last_location_of_maximum_calc
);

-- last_location_of_minimum_calc

CREATE OR REPLACE FUNCTION last_location_of_minimum_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_last_location_of_minimum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION last_location_of_minimum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'last_location_of_minimum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- last_location_of_minimum
CREATE OR REPLACE AGGREGATE last_location_of_minimum(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=last_location_of_minimum_calc
);

CREATE OR REPLACE AGGREGATE last_location_of_minimum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=last_location_of_minimum_calc
);

-- linear_trend_calc

CREATE OR REPLACE FUNCTION linear_trend_calc(state text_date_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_linear_trend_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION linear_trend_calc(state text_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'linear_trend_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- linear_trend
CREATE OR REPLACE AGGREGATE linear_trend(REAL, TIMESTAMP, TEXT[])
(
     SFUNC = text_date_1_p_agg,
     STYPE = text_date_1_p_type,
     COMBINEFUNC = combine_text_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=linear_trend_calc
);

CREATE OR REPLACE AGGREGATE linear_trend(REAL, TEXT[])
(
     SFUNC = text_1_p_agg,
     STYPE = text_1_p_type,
     COMBINEFUNC = combine_text_1_p,
     PARALLEL = SAFE,
     FINALFUNC=linear_trend_calc
);

-- longest_strike_above_mean_calc

CREATE OR REPLACE FUNCTION longest_strike_above_mean_calc(state value_date_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'ts_longest_strike_above_mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION longest_strike_above_mean_calc(state REAL[]) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'longest_strike_above_mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- longest_strike_above_mean
CREATE OR REPLACE AGGREGATE longest_strike_above_mean(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=longest_strike_above_mean_calc
);

CREATE OR REPLACE AGGREGATE longest_strike_above_mean(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=longest_strike_above_mean_calc
);

-- longest_strike_below_mean_calc

CREATE OR REPLACE FUNCTION longest_strike_below_mean_calc(state value_date_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'ts_longest_strike_below_mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION longest_strike_below_mean_calc(state REAL[]) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'longest_strike_below_mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- longest_strike_below_mean
CREATE OR REPLACE AGGREGATE longest_strike_below_mean(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=longest_strike_below_mean_calc
);

CREATE OR REPLACE AGGREGATE longest_strike_below_mean(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=longest_strike_below_mean_calc
);

-- maximum_calc
CREATE OR REPLACE FUNCTION maximum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'maximum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- maximum
CREATE OR REPLACE AGGREGATE maximum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=maximum_calc
);

-- mean_calc
CREATE OR REPLACE FUNCTION mean_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'mean_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- mean
CREATE OR REPLACE AGGREGATE mean(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=mean_calc
);

-- mean_abs_change_calc

CREATE OR REPLACE FUNCTION mean_abs_change_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_mean_abs_change_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION mean_abs_change_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'mean_abs_change_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- mean_abs_change
CREATE OR REPLACE AGGREGATE mean_abs_change(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=mean_abs_change_calc
);

CREATE OR REPLACE AGGREGATE mean_abs_change(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=mean_abs_change_calc
);

-- mean_change_calc

CREATE OR REPLACE FUNCTION mean_change_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_mean_change_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION mean_change_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'mean_change_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- mean_change
CREATE OR REPLACE AGGREGATE mean_change(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=mean_change_calc
);

CREATE OR REPLACE AGGREGATE mean_change(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=mean_change_calc
);

-- mean_n_absolute_max_calc

CREATE OR REPLACE FUNCTION mean_n_absolute_max_calc(state single_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'mean_n_absolute_max_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- mean_n_absolute_max
CREATE OR REPLACE AGGREGATE mean_n_absolute_max(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=mean_n_absolute_max_calc
);

-- mean_second_derivative_central_calc

CREATE OR REPLACE FUNCTION mean_second_derivative_central_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_mean_second_derivative_central_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION mean_second_derivative_central_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'mean_second_derivative_central_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- mean_second_derivative_central
CREATE OR REPLACE AGGREGATE mean_second_derivative_central(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=mean_second_derivative_central_calc
);

CREATE OR REPLACE AGGREGATE mean_second_derivative_central(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=mean_second_derivative_central_calc
);

-- median_calc

CREATE OR REPLACE FUNCTION median_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'median_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- median
CREATE OR REPLACE AGGREGATE median(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=median_calc
);

-- minimum_calc
CREATE OR REPLACE FUNCTION minimum_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'minimum_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- minimum
CREATE OR REPLACE AGGREGATE minimum(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=minimum_calc
);

-- mode_calc

CREATE OR REPLACE FUNCTION mode_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'mode_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- mode
CREATE OR REPLACE AGGREGATE mode(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=mode_calc
);

-- number_crossing_m_calc

CREATE OR REPLACE FUNCTION number_crossing_m_calc(state single_date_1_p_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'ts_number_crossing_m_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION number_crossing_m_calc(state single_1_p_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'number_crossing_m_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- number_crossing_m
CREATE OR REPLACE AGGREGATE number_crossing_m(REAL, TIMESTAMP, BIGINT)
(
     SFUNC = single_date_1_p_agg,
     STYPE = single_date_1_p_type,
     COMBINEFUNC = combine_single_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=number_crossing_m_calc
);

CREATE OR REPLACE AGGREGATE number_crossing_m(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=number_crossing_m_calc
);

-- number_cwt_peaks_calc

CREATE OR REPLACE FUNCTION number_cwt_peaks_calc(state single_date_1_p_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'ts_number_cwt_peaks_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION number_cwt_peaks_calc(state single_1_p_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'number_cwt_peaks_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- number_cwt_peaks
CREATE OR REPLACE AGGREGATE number_cwt_peaks(REAL, TIMESTAMP, BIGINT)
(
     SFUNC = single_date_1_p_agg,
     STYPE = single_date_1_p_type,
     COMBINEFUNC = combine_single_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=number_cwt_peaks_calc
);

CREATE OR REPLACE AGGREGATE number_cwt_peaks(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=number_cwt_peaks_calc
);

-- number_peaks_calc

CREATE OR REPLACE FUNCTION number_peaks_calc(state single_date_1_p_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'ts_number_peaks_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION number_peaks_calc(state single_1_p_type) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'number_peaks_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- number_peaks
CREATE OR REPLACE AGGREGATE number_peaks(REAL, TIMESTAMP, BIGINT)
(
     SFUNC = single_date_1_p_agg,
     STYPE = single_date_1_p_type,
     COMBINEFUNC = combine_single_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=number_peaks_calc
);

CREATE OR REPLACE AGGREGATE number_peaks(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=number_peaks_calc
);

-- num_unique_calc

CREATE OR REPLACE FUNCTION num_unique_calc(state REAL[]) RETURNS BIGINT as '/usr/local/lib/libpgetu', 'num_unique_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- num_unique
CREATE OR REPLACE AGGREGATE num_unique(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=num_unique_calc
);

-- partial_autocorrelation_calc

CREATE OR REPLACE FUNCTION partial_autocorrelation_calc(state list_date_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'ts_partial_autocorrelation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION partial_autocorrelation_calc(state list_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'partial_autocorrelation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- partial_autocorrelation
CREATE OR REPLACE AGGREGATE partial_autocorrelation(REAL, TIMESTAMP, BIGINT[])
(
     SFUNC = list_date_1_p_agg,
     STYPE = list_date_1_p_type,
     COMBINEFUNC = combine_list_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=partial_autocorrelation_calc
);

CREATE OR REPLACE AGGREGATE partial_autocorrelation(REAL, BIGINT[])
(
     SFUNC = list_1_p_agg,
     STYPE = list_1_p_type,
     COMBINEFUNC = combine_list_1_p,
     PARALLEL = SAFE,
     FINALFUNC=partial_autocorrelation_calc
);

-- percentage_of_reoccurring_datapoints_to_all_datapoints_calc

CREATE OR REPLACE FUNCTION percentage_of_reoccurring_datapoints_to_all_datapoints_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'percentage_of_reoccurring_datapoints_to_all_datapoints_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- percentage_of_reoccurring_datapoints_to_all_datapoints
CREATE OR REPLACE AGGREGATE percentage_of_reoccurring_datapoints_to_all_datapoints(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=percentage_of_reoccurring_datapoints_to_all_datapoints_calc
);

-- percentage_of_reoccurring_values_to_all_values_calc

CREATE OR REPLACE FUNCTION percentage_of_reoccurring_values_to_all_values_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'percentage_of_reoccurring_values_to_all_values_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- percentage_of_reoccurring_values_to_all_values
CREATE OR REPLACE AGGREGATE percentage_of_reoccurring_values_to_all_values(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=percentage_of_reoccurring_values_to_all_values_calc
);

-- quantile_calc

CREATE OR REPLACE FUNCTION quantile_calc(state single_float_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'quantile_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- quantile
CREATE OR REPLACE AGGREGATE quantile(REAL, REAL)
(
     SFUNC = single_float_1_p_agg,
     STYPE = single_float_1_p_type,
     COMBINEFUNC = combine_single_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=quantile_calc
);

-- range_count_calc
CREATE OR REPLACE FUNCTION range_count_calc(state double_float_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'range_count_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- range_count
CREATE OR REPLACE AGGREGATE range_count(REAL, REAL, REAL)
(
     SFUNC = double_float_1_p_agg,
     STYPE = double_float_1_p_type,
     COMBINEFUNC = combine_double_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=range_count_calc
);

-- ratio_beyond_r_sigma_calc

CREATE OR REPLACE FUNCTION ratio_beyond_r_sigma_calc(state single_float_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ratio_beyond_r_sigma_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- ratio_beyond_r_sigma
CREATE OR REPLACE AGGREGATE ratio_beyond_r_sigma(REAL, REAL)
(
     SFUNC = single_float_1_p_agg,
     STYPE = single_float_1_p_type,
     COMBINEFUNC = combine_single_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=ratio_beyond_r_sigma_calc
);

-- ratio_value_number_to_time_series_length_calc

CREATE OR REPLACE FUNCTION ratio_value_number_to_time_series_length_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'ratio_value_number_to_time_series_length_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- ratio_value_number_to_time_series_length
CREATE OR REPLACE AGGREGATE ratio_value_number_to_time_series_length(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=ratio_value_number_to_time_series_length_calc
);

-- root_mean_square_calc

CREATE OR REPLACE FUNCTION root_mean_square_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'root_mean_square_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- root_mean_square
CREATE OR REPLACE AGGREGATE root_mean_square(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=root_mean_square_calc
);

-- sample_entropy_calc

CREATE OR REPLACE FUNCTION sample_entropy_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_sample_entropy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION sample_entropy_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'sample_entropy_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- sample_entropy
CREATE OR REPLACE AGGREGATE sample_entropy(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=sample_entropy_calc
);

CREATE OR REPLACE AGGREGATE sample_entropy(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=sample_entropy_calc
);

-- skew_calc

CREATE OR REPLACE FUNCTION skew_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'skew_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- skew
CREATE OR REPLACE AGGREGATE skew(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=skew_calc
);

-- standard_deviation_calc
CREATE OR REPLACE FUNCTION standard_deviation_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'standard_deviation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- standard_deviation
CREATE OR REPLACE AGGREGATE standard_deviation(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=standard_deviation_calc
);

-- sum_of_reoccurring_datapoints_calc

CREATE OR REPLACE FUNCTION sum_of_reoccurring_datapoints_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'sum_of_reoccurring_datapoints_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- sum_of_reoccurring_datapoints
CREATE OR REPLACE AGGREGATE sum_of_reoccurring_datapoints(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=sum_of_reoccurring_datapoints_calc
);

-- sum_of_reoccurring_values_calc

CREATE OR REPLACE FUNCTION sum_of_reoccurring_values_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'sum_of_reoccurring_values_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- sum_of_reoccurring_values
CREATE OR REPLACE AGGREGATE sum_of_reoccurring_values(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=sum_of_reoccurring_values_calc
);

-- sum_values_calc
CREATE OR REPLACE FUNCTION sum_values_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'sum_values_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- sum_values
CREATE OR REPLACE AGGREGATE sum_values(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=sum_values_calc
);

-- symmetry_looking_calc

CREATE OR REPLACE FUNCTION symmetry_looking_calc(state float_1_p_type) RETURNS JSONB as '/usr/local/lib/libpgetu', 'symmetry_looking_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- symmetry_looking
CREATE OR REPLACE AGGREGATE symmetry_looking(REAL, REAL[])
(
     SFUNC = float_1_p_agg,
     STYPE = float_1_p_type,
     COMBINEFUNC = combine_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=symmetry_looking_calc
);

-- time_reversal_asymmetry_statistic_calc

CREATE OR REPLACE FUNCTION time_reversal_asymmetry_statistic_calc(state single_date_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_time_reversal_asymmetry_statistic_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION time_reversal_asymmetry_statistic_calc(state single_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'time_reversal_asymmetry_statistic_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- time_reversal_asymmetry_statistic
CREATE OR REPLACE AGGREGATE time_reversal_asymmetry_statistic(REAL, TIMESTAMP, BIGINT)
(
     SFUNC = single_date_1_p_agg,
     STYPE = single_date_1_p_type,
     COMBINEFUNC = combine_single_date_1_p,
     PARALLEL = SAFE,
     FINALFUNC=time_reversal_asymmetry_statistic_calc
);

CREATE OR REPLACE AGGREGATE time_reversal_asymmetry_statistic(REAL, BIGINT)
(
     SFUNC = single_1_p_agg,
     STYPE = single_1_p_type,
     COMBINEFUNC = combine_single_1_p,
     PARALLEL = SAFE,
     FINALFUNC=time_reversal_asymmetry_statistic_calc
);

-- total_change_calc

CREATE OR REPLACE FUNCTION total_change_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'ts_total_change_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION total_change_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'total_change_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- total_change
CREATE OR REPLACE AGGREGATE total_change(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=total_change_calc
);

CREATE OR REPLACE AGGREGATE total_change(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=total_change_calc
);

-- tsfirst_calc

CREATE OR REPLACE FUNCTION tsfirst_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'tsfirst_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION tsfirst_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'first_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- tsfirst
CREATE OR REPLACE AGGREGATE tsfirst(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=tsfirst_calc
);

CREATE OR REPLACE AGGREGATE tsfirst(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAl[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=tsfirst_calc
);

-- tslast_calc

CREATE OR REPLACE FUNCTION tslast_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'tslast_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

CREATE OR REPLACE FUNCTION tslast_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'last_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- tslast
CREATE OR REPLACE AGGREGATE tslast(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=tslast_calc
);

CREATE OR REPLACE AGGREGATE tslast(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAl[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=tslast_calc
);

-- value_count_calc
CREATE OR REPLACE FUNCTION value_count_calc(state single_float_1_p_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'value_count_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- value_count
CREATE OR REPLACE AGGREGATE value_count(REAL, REAL)
(
     SFUNC = single_float_1_p_agg,
     STYPE = single_float_1_p_type,
     COMBINEFUNC = combine_single_float_1_p,
     PARALLEL = SAFE,
     FINALFUNC=value_count_calc
);

-- variance_calc
CREATE OR REPLACE FUNCTION variance_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'variance_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- variance
CREATE OR REPLACE AGGREGATE variance(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=variance_calc
);

-- variance_larger_than_standard_deviation_calc

CREATE OR REPLACE FUNCTION variance_larger_than_standard_deviation_calc(state REAL[]) RETURNS BOOLEAN as '/usr/local/lib/libpgetu', 'variance_larger_than_standard_deviation_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- variance_larger_than_standard_deviation
CREATE OR REPLACE AGGREGATE variance_larger_than_standard_deviation(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=variance_larger_than_standard_deviation_calc
);

-- variation_coefficient_calc

CREATE OR REPLACE FUNCTION variation_coefficient_calc(state REAL[]) RETURNS REAL as '/usr/local/lib/libpgetu', 'variation_coefficient_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- variation_coefficient
CREATE OR REPLACE AGGREGATE variation_coefficient(REAL)
(
     SFUNC = ARRAY_APPEND,
     STYPE = REAL[],
     COMBINEFUNC = ARRAY_CAT,
     PARALLEL = SAFE,
     FINALFUNC=variation_coefficient_calc
);

-- velocity_calc

CREATE OR REPLACE FUNCTION velocity_calc(state value_date_type) RETURNS REAL as '/usr/local/lib/libpgetu', 'velocity_calc' LANGUAGE C STRICT IMMUTABLE PARALLEL SAFE;

-- velocity
CREATE OR REPLACE AGGREGATE velocity(REAL, TIMESTAMP)
(
     SFUNC = value_date_agg,
     STYPE = value_date_type,
     COMBINEFUNC = combine_value_date,
     PARALLEL = SAFE,
     FINALFUNC=velocity_calc
);

