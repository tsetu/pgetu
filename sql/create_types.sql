-- feature_return
CREATE TYPE feature_return AS (
    features TEXT[]
    ,values REAL[]
);

-- value_date_type
CREATE TYPE value_date_type AS (
    list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_value_date
CREATE OR REPLACE FUNCTION combine_value_date(state1 value_date_type, state2 value_date_type)
RETURNS value_date_type
AS
$$
BEGIN
    RETURN (ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- value_date_agg
CREATE OR REPLACE FUNCTION value_date_agg(state value_date_type, next REAL, nextdate TIMESTAMP)
RETURNS value_date_type
AS
$$
BEGIN
    RETURN (array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_1_p_type
CREATE TYPE single_1_p_type AS (
    parameter BIGINT
    ,list REAL[]
);

-- combine_single_1_p
CREATE OR REPLACE FUNCTION combine_single_1_p(state1 single_1_p_type, state2 single_1_p_type)
RETURNS single_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_1_p_agg
CREATE OR REPLACE FUNCTION single_1_p_agg(state single_1_p_type, next REAL, parameter BIGINT)
RETURNS single_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_date_1_p_type
CREATE TYPE single_date_1_p_type AS (
    parameter BIGINT
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_single_date_1_p
CREATE OR REPLACE FUNCTION combine_single_date_1_p(state1 single_date_1_p_type, state2 single_date_1_p_type)
RETURNS single_date_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_date_1_p_agg
CREATE OR REPLACE FUNCTION single_date_1_p_agg(state single_date_1_p_type, next REAL, nextdate TIMESTAMP, parameter BIGINT)
RETURNS single_date_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- list_1_p_type
CREATE TYPE list_1_p_type AS (
    parameter BIGINT[]
    ,list REAL[]
);

-- combine_list_1_p
CREATE OR REPLACE FUNCTION combine_list_1_p(state1 list_1_p_type, state2 list_1_p_type)
RETURNS list_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- list_1_p_agg
CREATE OR REPLACE FUNCTION list_1_p_agg(state list_1_p_type, next REAL, parameter BIGINT[])
RETURNS list_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- list_date_1_p_type
CREATE TYPE list_date_1_p_type AS (
    parameter BIGINT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_list_date_1_p
CREATE OR REPLACE FUNCTION combine_list_date_1_p(state1 list_date_1_p_type, state2 list_date_1_p_type)
RETURNS list_date_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- list_date_1_p_agg
CREATE OR REPLACE FUNCTION list_date_1_p_agg(state list_date_1_p_type, next REAL, nextdate TIMESTAMP, parameter BIGINT[])
RETURNS list_date_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- float_1_p_type
CREATE TYPE float_1_p_type AS (
    parameter REAL[]
    ,list REAL[]
);

-- combine_float_1_p
CREATE OR REPLACE FUNCTION combine_float_1_p(state1 float_1_p_type, state2 float_1_p_type)
RETURNS float_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- float_1_p_agg
CREATE OR REPLACE FUNCTION float_1_p_agg(state float_1_p_type, next REAL, parameter REAL[])
RETURNS float_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- float_date_1_p_type
CREATE TYPE float_date_1_p_type AS (
    parameter REAL[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_float_date_1_p
CREATE OR REPLACE FUNCTION combine_float_date_1_p(state1 float_date_1_p_type, state2 float_date_1_p_type)
RETURNS float_date_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- float_date_1_p_agg
CREATE OR REPLACE FUNCTION float_date_1_p_agg(state float_date_1_p_type, next REAL, nextdate TIMESTAMP, parameter REAL[])
RETURNS float_date_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- double_float_1_p_type
CREATE TYPE double_float_1_p_type AS (
    parameter1 REAL
    ,parameter2 REAL
    ,list REAL[]
);

-- combine_double_float_1_p
CREATE OR REPLACE FUNCTION combine_double_float_1_p(state1 double_float_1_p_type, state2 double_float_1_p_type)
RETURNS double_float_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter1, state1.parameter2, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- double_float_1_p_agg
CREATE OR REPLACE FUNCTION double_float_1_p_agg(state double_float_1_p_type, next REAL, parameter1 REAL, parameter2 REAL)
RETURNS double_float_1_p_type
AS
$$
BEGIN
    RETURN (parameter1, parameter2, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_float_1_p_type
CREATE TYPE single_float_1_p_type AS (
    parameter REAL
    ,list REAL[]
);

-- combine_single_float_1_p
CREATE OR REPLACE FUNCTION combine_single_float_1_p(state1 single_float_1_p_type, state2 single_float_1_p_type)
RETURNS single_float_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_float_1_p_agg
CREATE OR REPLACE FUNCTION single_float_1_p_agg(state single_float_1_p_type, next REAL, parameter REAL)
RETURNS single_float_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_date_float_1_p_type
CREATE TYPE single_date_float_1_p_type AS (
    parameter REAL
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_single_date_float_1_p
CREATE OR REPLACE FUNCTION combine_single_date_float_1_p(state1 single_date_float_1_p_type, state2 single_date_float_1_p_type)
RETURNS single_date_float_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- single_date_float_1_p_agg
CREATE OR REPLACE FUNCTION single_date_float_1_p_agg(state single_date_float_1_p_type, next REAL, nextdate TIMESTAMP, parameter REAL)
RETURNS single_date_float_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- text_1_p_type
CREATE TYPE text_1_p_type AS (
    parameter TEXT[]
    ,list REAL[]
);

-- combine_text_1_p
CREATE OR REPLACE FUNCTION combine_text_1_p(state1 text_1_p_type, state2 text_1_p_type)
RETURNS text_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- text_1_p_agg
CREATE OR REPLACE FUNCTION text_1_p_agg(state text_1_p_type, next REAL, parameter TEXT[])
RETURNS text_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- text_date_1_p_type
CREATE TYPE text_date_1_p_type AS (
    parameter TEXT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_text_date_1_p
CREATE OR REPLACE FUNCTION combine_text_date_1_p(state1 text_date_1_p_type, state2 text_date_1_p_type)
RETURNS text_date_1_p_type
AS
$$
BEGIN
    RETURN (state1.parameter, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- text_date_1_p_agg
CREATE OR REPLACE FUNCTION text_date_1_p_agg(state text_date_1_p_type, next REAL, nextdate TIMESTAMP, parameter TEXT[])
RETURNS text_date_1_p_type
AS
$$
BEGIN
    RETURN (parameter, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- agg_autocorrelation_type
CREATE TYPE agg_autocorrelation_type AS (
    f_agg TEXT[]
    ,maxlag BIGINT[]
    ,list REAL[]
);

-- combine_agg_autocorrelation
CREATE OR REPLACE FUNCTION combine_agg_autocorrelation(state1 agg_autocorrelation_type, state2 agg_autocorrelation_type)
RETURNS agg_autocorrelation_type
AS
$$
BEGIN
    RETURN (state1.f_agg, state1.maxlag, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- agg_autocorrelation_agg
CREATE OR REPLACE FUNCTION agg_autocorrelation_agg(state agg_autocorrelation_type, next REAL, f_agg TEXT[], maxlag BIGINT[])
RETURNS agg_autocorrelation_type
AS
$$
BEGIN
    RETURN (f_agg, maxlag, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- agg_autocorrelation_type
CREATE TYPE ts_agg_autocorrelation_type AS (
    f_agg TEXT[]
    ,maxlag BIGINT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_ts_agg_autocorrelation
CREATE OR REPLACE FUNCTION combine_ts_agg_autocorrelation(state1 ts_agg_autocorrelation_type, state2 ts_agg_autocorrelation_type)
RETURNS ts_agg_autocorrelation_type
AS
$$
BEGIN
    RETURN (state1.f_agg, state1.maxlag, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- ts_agg_autocorrelation_agg
CREATE OR REPLACE FUNCTION ts_agg_autocorrelation_agg(state ts_agg_autocorrelation_type, next REAL, nextdate TIMESTAMP, f_agg TEXT[], maxlag BIGINT[])
RETURNS ts_agg_autocorrelation_type
AS
$$
BEGIN
    RETURN (f_agg, maxlag, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_coeff_type
CREATE TYPE fft_coeff_type AS (
    coeff BIGINT[]
    ,attr TEXT[]
    ,list REAL[]
);

-- combine_fft_coeff
CREATE OR REPLACE FUNCTION combine_fft_coeff(state1 fft_coeff_type, state2 fft_coeff_type)
RETURNS fft_coeff_type
AS
$$
BEGIN
    RETURN (state1.coeff, state1.attr, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_coeff_agg
CREATE OR REPLACE FUNCTION fft_coeff_agg(state fft_coeff_type, next REAL, coeff BIGINT[], attr TEXT[])
RETURNS fft_coeff_type
AS
$$
BEGIN
    RETURN (coeff, attr, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- ts_fft_coeff_type
CREATE TYPE ts_fft_coeff_type AS (
    coeff BIGINT[]
    ,attr TEXT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_fft_coeff
CREATE OR REPLACE FUNCTION combine_ts_fft_coeff(state1 ts_fft_coeff_type, state2 ts_fft_coeff_type)
RETURNS ts_fft_coeff_type
AS
$$
BEGIN
    RETURN (state1.coeff, state1.attr, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_coeff_agg
CREATE OR REPLACE FUNCTION ts_fft_coeff_agg(state ts_fft_coeff_type, next REAL, nextdate TIMESTAMP, coeff BIGINT[], attr TEXT[])
RETURNS ts_fft_coeff_type
AS
$$
BEGIN
    RETURN (coeff, attr, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_aggregated_type
CREATE TYPE fft_aggregated_type AS (
    aggtype TEXT[]
    ,list REAL[]
);

-- combine_fft_aggregated
CREATE OR REPLACE FUNCTION combine_fft_aggregated(state1 fft_aggregated_type, state2 fft_aggregated_type)
RETURNS fft_aggregated_type
AS
$$
BEGIN
    RETURN (state1.aggtype, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_aggregated_agg
CREATE OR REPLACE FUNCTION fft_aggregated_agg(state fft_aggregated_type, next REAL, aggtype TEXT[])
RETURNS fft_aggregated_type
AS
$$
BEGIN
    RETURN (aggtype, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_aggregated_type
CREATE TYPE ts_fft_aggregated_type AS (
    aggtype TEXT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_fft_aggregated
CREATE OR REPLACE FUNCTION combine_ts_fft_aggregated(state1 ts_fft_aggregated_type, state2 ts_fft_aggregated_type)
RETURNS ts_fft_aggregated_type
AS
$$
BEGIN
    RETURN (state1.aggtype, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- fft_aggregated_agg
CREATE OR REPLACE FUNCTION ts_fft_aggregated_agg(state ts_fft_aggregated_type, next REAL, nextdate TIMESTAMP, aggtype TEXT[])
RETURNS ts_fft_aggregated_type
AS
$$
BEGIN
    RETURN (aggtype, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- approx_entropy_type
CREATE TYPE approx_entropy_type AS (
    m BIGINT
    ,r REAL
    ,list REAL[]
);

-- combine_approx_entropy
CREATE OR REPLACE FUNCTION combine_approx_entropy(state1 approx_entropy_type, state2 approx_entropy_type)
RETURNS approx_entropy_type
AS
$$
BEGIN
    RETURN (state1.m, state1.r, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- approx_entropy_agg
CREATE OR REPLACE FUNCTION approx_entropy_agg(state approx_entropy_type, next REAL, m BIGINT, r REAL)
RETURNS approx_entropy_type
AS
$$
BEGIN
    RETURN (m, r, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- approx_entropy_type
CREATE TYPE ts_approx_entropy_type AS (
    m BIGINT
    ,r REAL
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_ts_approx_entropy
CREATE OR REPLACE FUNCTION combine_ts_approx_entropy(state1 ts_approx_entropy_type, state2 ts_approx_entropy_type)
RETURNS ts_approx_entropy_type
AS
$$
BEGIN
    RETURN (state1.m, state1.r, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- ts_approx_entropy_agg
CREATE OR REPLACE FUNCTION ts_approx_entropy_agg(state ts_approx_entropy_type, next REAL, nextdate TIMESTAMP, m BIGINT, r REAL)
RETURNS ts_approx_entropy_type
AS
$$
BEGIN
    RETURN (m, r, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- energy_ratio_type
CREATE TYPE energy_ratio_type AS (
    num_segments BIGINT[]
    ,segment_focus BIGINT[]
    ,list REAL[]
);

-- combine_energy_ratio
CREATE OR REPLACE FUNCTION combine_energy_ratio(state1 energy_ratio_type, state2 energy_ratio_type)
RETURNS energy_ratio_type
AS
$$
BEGIN
    RETURN (state1.num_segments, state1.segment_focus, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- energy_ratio_agg
CREATE OR REPLACE FUNCTION energy_ratio_agg(state energy_ratio_type, next REAL, num_segments BIGINT[], segment_focus BIGINT[])
RETURNS energy_ratio_type
AS
$$
BEGIN
    RETURN (num_segments, segment_focus, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- ts_energy_ratio_type
CREATE TYPE ts_energy_ratio_type AS (
    num_segments BIGINT[]
    ,segment_focus BIGINT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_ts_energy_ratio
CREATE OR REPLACE FUNCTION combine_ts_energy_ratio(state1 ts_energy_ratio_type, state2 ts_energy_ratio_type)
RETURNS ts_energy_ratio_type
AS
$$
BEGIN
    RETURN (state1.num_segments, state1.segment_focus, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- ts_energy_ratio_agg
CREATE OR REPLACE FUNCTION ts_energy_ratio_agg(state ts_energy_ratio_type, next REAL, nextdate TIMESTAMP, num_segments BIGINT[], segment_focus BIGINT[])
RETURNS ts_energy_ratio_type
AS
$$
BEGIN
    RETURN (num_segments, segment_focus, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- agg_dickey_type
CREATE TYPE agg_dickey_type AS (
    attr TEXT[]
    ,autolag TEXT[]
    ,list REAL[]
);

-- combine_agg_dickey
CREATE OR REPLACE FUNCTION combine_agg_dickey(state1 agg_dickey_type, state2 agg_dickey_type)
RETURNS agg_dickey_type
AS
$$
BEGIN
    RETURN (state1.attr, state1.autolag, ARRAY_CAT(state1.list, state2.list));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- agg_dickey_agg
CREATE OR REPLACE FUNCTION agg_dickey_agg(state agg_dickey_type, next REAL, attr TEXT[], autolag TEXT[])
RETURNS agg_dickey_type
AS
$$
BEGIN
    RETURN (attr, autolag, array_append(state.list, next));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- agg_dickey_type
CREATE TYPE ts_agg_dickey_type AS (
    attr TEXT[]
    ,autolag TEXT[]
    ,list REAL[]
    ,dates TIMESTAMP[]
);

-- combine_ts_agg_dickey
CREATE OR REPLACE FUNCTION combine_ts_agg_dickey(state1 ts_agg_dickey_type, state2 ts_agg_dickey_type)
RETURNS ts_agg_dickey_type
AS
$$
BEGIN
    RETURN (state1.attr, state1.autolag, ARRAY_CAT(state1.list, state2.list), ARRAY_CAT(state1.dates, state2.dates));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

-- ts_agg_dickey_agg
CREATE OR REPLACE FUNCTION ts_agg_dickey_agg(state ts_agg_dickey_type, next REAL, nextdate TIMESTAMP, attr TEXT[], autolag TEXT[])
RETURNS ts_agg_dickey_type
AS
$$
BEGIN
    RETURN (attr, autolag, array_append(state.list, next), array_append(state.dates, nextdate));
END;
$$ LANGUAGE PLPGSQL IMMUTABLE PARALLEL SAFE;

