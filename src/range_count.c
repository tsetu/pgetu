#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <pgtypes_numeric.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(range_count_calc);

Datum
range_count_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: range_count
  Input Type: double_float_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength;
  bool *arrayNulls, p1Null, p2Null;
  double *array, parameter1, parameter2;
  int parse_res;

  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_double_float_1_p_type(&t, &array, &arrayLength, &arrayNulls,
					  &parameter1, &p1Null,
					  &parameter2, &p2Null);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(range_count(array, arrayLength, parameter1, parameter2));

}
