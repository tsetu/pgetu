#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_index_mass_quantile_calc);

Datum
ts_index_mass_quantile_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: index_mass_quantile
  Input Type: float_1_p_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength, paramLength;
  bool *arrayNulls, *dateNulls, *paramNulls;
  double *array, *param;
  Datum *dates;
  int parse_res;

  double *abs_x;
  double sum, cummulative, result;
  int i, p;

  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_float_date_1_p_type(&t, &array, &arrayLength, &arrayNulls,
					&dates, &dateLength, &dateNulls,
					&param, &paramLength, &paramNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(paramLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[paramLength];
  index_mass_quantile(array, arrayLength, param, paramLength, results);
  
  for (p=0; p<paramLength; p++) {
    int argmax, ret;
    char *k, *fmtstr, *res;
    k = (char *)palloc(16*sizeof(char));
    res = (char *)palloc(32*sizeof(char));
    
    /* Create the key */
    create_fmtstr(&fmtstr, param[p]);
    ret = snprintf(k, 16, fmtstr, param[p]);
    pfree(fmtstr);
    pairs[p].key.type = jbvString;
    pairs[p].key.val.string.len = 16;
    pairs[p].key.val.string.val = k;

    /* Create the value */
    create_fmtstr(&fmtstr, results[p].value);
    ret = snprintf(res, 32, fmtstr, results[p].value);
    pfree(fmtstr);
    
    pairs[p].value.type = jbvString;
    pairs[p].value.val.string.len = 32;
    pairs[p].value.val.string.val = res;

  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = paramLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));

}


PG_FUNCTION_INFO_V1(index_mass_quantile_calc);

Datum
index_mass_quantile_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: index_mass_quantile
  Input Type: float_1_p_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, paramLength;
  bool *arrayNulls, *paramNulls;
  double *array, *param;
  int parse_res;

  double *abs_x;
  double sum, cummulative, result;
  int i, p;

  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_float_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				   &param, &paramLength, &paramNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(paramLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[paramLength];
  index_mass_quantile(array, arrayLength, param, paramLength, results);
  
  for (p=0; p<paramLength; p++) {
    int argmax, ret;
    char *k, *fmtstr, *res;
    k = (char *)palloc(16*sizeof(char));
    res = (char *)palloc(32*sizeof(char));
    
    /* Create the key */
    create_fmtstr(&fmtstr, param[p]);
    ret = snprintf(k, 16, fmtstr, param[p]);
    pfree(fmtstr);
    pairs[p].key.type = jbvString;
    pairs[p].key.val.string.len = 16;
    pairs[p].key.val.string.val = k;

    /* Create the value */
    create_fmtstr(&fmtstr, results[p].value);
    ret = snprintf(res, 32, fmtstr, results[p].value);
    pfree(fmtstr);
    
    pairs[p].value.type = jbvString;
    pairs[p].value.val.string.len = 32;
    pairs[p].value.val.string.val = res;

  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = paramLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));

}
