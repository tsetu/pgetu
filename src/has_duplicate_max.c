#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(has_duplicate_max_calc);

Datum
has_duplicate_max_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: has_duplicate_max
  Input Type: REAL[]
  Return Type: BOOLEAN
  ***************************************************************************

  **************************************************************************/
  ArrayType *a = PG_GETARG_ARRAYTYPE_P(0);
  int arrayLength;
  bool *arrayNulls;
  double *array;
  int parse_res, i, num_max;
  double max;
  bool result;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_postgres_double_array(a, &array, &arrayLength, &arrayNulls);

  if (!parse_res) {
     PG_RETURN_NULL();
  }

  /* Calculate the number of observations with the max value */
  PG_RETURN_BOOL(has_duplicate_max(array, arrayLength));
}
