#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_mean_abs_change_calc);

Datum
ts_mean_abs_change_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: mean_abs_change
  Input Type: value_date_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength;
  bool *arrayNulls, *dateNulls;
  double *array;
  Datum *dates;
  int parse_res;
  double *x, result;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_value_date_type(&t, &array, &arrayLength, &arrayNulls,
                          &dates, &dateLength, &dateNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(mean_abs_change(array, arrayLength));
}

PG_FUNCTION_INFO_V1(mean_abs_change_calc);

Datum
mean_abs_change_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: mean_abs_change
  Input Type: value_date_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  ArrayType *a = PG_GETARG_ARRAYTYPE_P(0);
  int arrayLength;
  bool *arrayNulls;
  double *array;
  int parse_res;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_postgres_double_array(a, &array, &arrayLength, &arrayNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(mean_abs_change(array, arrayLength));
}
