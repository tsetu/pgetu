#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <math.h>
#include <etu.h>
#include "tsutils.h"


PG_FUNCTION_INFO_V1(ts_agg_autocorrelation_calc);

Datum
ts_agg_autocorrelation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: agg_autocorrelation
  Input Type: agg_autocorrelation_type
  Return Type: feature_return
  ***************************************************************************
  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, dateLength, mLength, fLength;
  bool *arrayNulls, *dateNulls, *mNulls, *fNulls;
  double *array;
  Datum *dates;
  int *maxlags;
  char **f_aggs;
  int *f_aggs_length;
  int parse_res, f;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_ts_agg_autocorrelation_type(&t, &array,
						&arrayLength, &arrayNulls,
						&dates, &dateLength, &dateNulls,
						&maxlags, &mLength, &mNulls,
						&f_aggs, &f_aggs_length,
						&fLength, &fNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(fLength*sizeof(JsonbPair));

  /* Create the AGG_AUTOC type to hold parameters */
  ETU_RETURN results[fLength];
  AGG_AUTOC params[fLength];

  for (f=0; f<fLength; f++) {
    params[f].maxlag = maxlags[f];
    strncpy(params[f].f_agg, f_aggs[f], NAME_LENGTH);
  }

  /* Calculate the agg autocorrelation */
  agg_autocorrelation(array, arrayLength, params, fLength, results);

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (f=0; f<fLength; f++) {
    char *key, *res, *fmtstr;
    int key_len = 16+f_aggs_length[f]+floor(log10(abs(maxlags[f])));
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    int ret = snprintf(key, key_len, "f_agg_%s_maxlag_%d", f_aggs[f], maxlags[f]);
    pairs[f].key.type = jbvString;
    pairs[f].key.val.string.len = key_len;
    pairs[f].key.val.string.val = key;

    /* Save the value */

    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[f].value);
    ret = snprintf(res, 32, fmtstr, results[f].value);

    pairs[f].value.type = jbvString;
    pairs[f].value.val.string.len = 32;
    pairs[f].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = fLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}

PG_FUNCTION_INFO_V1(agg_autocorrelation_calc);

Datum
agg_autocorrelation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: agg_autocorrelation
  Input Type: agg_autocorrelation_type
  Return Type: feature_return
  ***************************************************************************
  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, mLength, fLength;
  bool *arrayNulls, *mNulls, *fNulls;
  double *array;
  int *maxlags;
  char **f_aggs;
  int *f_aggs_length;
  int parse_res, f;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_agg_autocorrelation_type(&t, &array,
					     &arrayLength, &arrayNulls,
					     &maxlags, &mLength, &mNulls,
					     &f_aggs, &f_aggs_length,
					     &fLength, &fNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(fLength*sizeof(JsonbPair));

  /* Create the AGG_AUTOC type to hold parameters */
  ETU_RETURN results[fLength];
  AGG_AUTOC params[fLength];

  for (f=0; f<fLength; f++) {
    params[f].maxlag = maxlags[f];
    strncpy(params[f].f_agg, f_aggs[f], NAME_LENGTH);
  }

  /* Calculate the agg autocorrelation */
  agg_autocorrelation(array, arrayLength, params, fLength, results);

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (f=0; f<fLength; f++) {
    char *key, *res, *fmtstr;
    int key_len = 16+f_aggs_length[f]+floor(log10(abs(maxlags[f])));
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    int ret = snprintf(key, key_len, "f_agg_%s_maxlag_%d", f_aggs[f], maxlags[f]);
    pairs[f].key.type = jbvString;
    pairs[f].key.val.string.len = key_len;
    pairs[f].key.val.string.val = key;

    /* Save the value */

    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[f].value);
    ret = snprintf(res, 32, fmtstr, results[f].value);

    pairs[f].value.type = jbvString;
    pairs[f].value.val.string.len = 32;
    pairs[f].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = fLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}

