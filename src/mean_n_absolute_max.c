#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(mean_n_absolute_max_calc);

Datum
mean_n_absolute_max_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: mean_n_absolute_max
  Input Type: single_date_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, parameter;
  bool *arrayNulls, pNull;
  double *array;
  int parse_res;

  /* Parse the __type for array, dates and parameter, 
   * return sorted by date */
  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				    &parameter, &pNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* If lag is bigger than the array, return Null */
  if (arrayLength < parameter) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(mean_n_absolute_max(array, arrayLength, parameter));
}
