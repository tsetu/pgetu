#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <datatype/timestamp.h>
#include <utils/timestamp.h>
#include <pgtypes_timestamp.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(velocity_calc);

Datum
velocity_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: velocity
  Input Type: value_date_type
  Return Type: REAL
  ***************************************************************************
  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength, i;
  bool *arrayNulls, *dateNulls;
  double *array;
  Datum *dates;
  int parse_res;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_value_date_type(&t, &array, &arrayLength, &arrayNulls,
				    &dates, &dateLength, &dateNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  time_t times[arrayLength];

  /* TODO: Convert from timestamp to time_t */
  for (i=0; i<arrayLength; i++) {
    times[i] = (time_t) ( (DatumGetTimestamp(dates[i])/USECS_PER_SEC) +
			  ((POSTGRES_EPOCH_JDATE - UNIX_EPOCH_JDATE) *
			   SECS_PER_DAY) );
  }
  
  PG_RETURN_FLOAT4(velocity(array, arrayLength, times));
}
