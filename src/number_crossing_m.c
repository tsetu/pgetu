#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_number_crossing_m_calc);

Datum
ts_number_crossing_m_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: number_crossing_m
  Input Type: single_date_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength, parameter;
  bool *arrayNulls, *dateNulls, pNull;
  double *array;
  Datum *dates;
  int parse_res;

  /* Parse the __type for array, dates and parameter, 
   * return sorted by date */
  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_date_1_p_type(&t, &array, &arrayLength, &arrayNulls,
					 &dates, &dateLength, &dateNulls,
					 &parameter, &pNull);

  
  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_INT64(number_crossing_m(array, arrayLength, parameter));
}

PG_FUNCTION_INFO_V1(number_crossing_m_calc);

Datum
number_crossing_m_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: number_crossing_m
  Input Type: single_date_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, parameter;
  bool *arrayNulls, pNull;
  double *array;
  int parse_res;

  /* Parse the __type for array, dates and parameter, 
   * return sorted by date */
  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				    &parameter, &pNull);

  
  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_INT64(number_crossing_m(array, arrayLength, parameter));
}
