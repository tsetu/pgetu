#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <pgtypes_numeric.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(symmetry_looking_calc);

Datum
symmetry_looking_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: symmetry_looking
  Input Type: float_1_p_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, paramLength;
  bool *arrayNulls, *paramNulls;
  double *array, *param;
  int parse_res, p;

  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_float_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				   &param, &paramLength, &paramNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(paramLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[paramLength];
  symmetry_looking(array, arrayLength, param, paramLength, results);
  
  for (p=0; p<paramLength; p++) {
    char *k, *fmtstr;
    k = (char *)palloc(16*sizeof(char));
    
    /* Create the key */
    create_fmtstr(&fmtstr, param[p]);
    int ret = snprintf(k, 16, fmtstr, param[p]);
    pfree(fmtstr);
    pairs[p].key.type = jbvString;
    pairs[p].key.val.string.len = 16;
    pairs[p].key.val.string.val = k;

    /* Create the value */
    pairs[p].value.type = jbvBool;
    pairs[p].value.val.boolean = (bool)results[p].value;

  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = paramLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}

