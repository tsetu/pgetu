#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <math.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_fft_coefficient_calc);

Datum
ts_fft_coefficient_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: fft_coefficient
  Input Type: fft_coeff_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, dateLength, cLength, aLength;
  bool *arrayNulls, *dateNulls, *cNulls, *aNulls;
  double *array;
  Datum *dates;
  int *coeffs;
  char **attrs;
  int *attr_lengths;
  int parse_res;

  int c;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_ts_fft_coef_type(&t, &array, &arrayLength, &arrayNulls,
				     &dates, &dateLength, &dateNulls,
				     &coeffs, &cLength, &cNulls,
				     &attrs, &attr_lengths,
				     &aLength, &aNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(cLength*sizeof(JsonbPair));

  /* Calculate the FFT Coeff */
  ETU_RETURN results[cLength];
  FFT_COEF params[cLength];

  for (c=0; c<cLength; c++) {
    strncpy(params[c].attr, attrs[c], NAME_LENGTH);
    params[c].coeff = coeffs[c];
  }

  fft_coefficient(array, arrayLength, params, cLength, results);
  
  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (c=0; c<cLength; c++) {
    char *key, *res, *fmtstr;
    int key_len, digits, ret;
    
    if ((0 <= coeffs[c]) && (coeffs[c] < 10))
      digits = 1;
    else
      digits = floor(log10(abs(coeffs[c])));
    key_len = 14+attr_lengths[c]+digits;
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    ret = snprintf(key, key_len, "attr_%s_coeff_%d", attrs[c], coeffs[c]);
    pairs[c].key.type = jbvString;
    pairs[c].key.val.string.len = key_len;
    pairs[c].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[c].value);
    ret = snprintf(res, 32, fmtstr, results[c].value);

    pairs[c].value.type = jbvString;
    pairs[c].value.val.string.len = 32;
    pairs[c].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = cLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}


PG_FUNCTION_INFO_V1(fft_coefficient_calc);

Datum
fft_coefficient_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: fft_coefficient
  Input Type: fft_coeff_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, cLength, aLength;
  bool *arrayNulls, *cNulls, *aNulls;
  double *array;
  int *coeffs;
  char **attrs;
  int *attr_lengths;
  int parse_res;

  int c;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_fft_coef_type(&t, &array, &arrayLength, &arrayNulls,
				  &coeffs, &cLength, &cNulls,
				  &attrs, &attr_lengths,
				  &aLength, &aNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(cLength*sizeof(JsonbPair));

  /* Calculate the FFT Coeff */
  ETU_RETURN results[cLength];
  FFT_COEF params[cLength];

  for (c=0; c<cLength; c++) {
    strncpy(params[c].attr, attrs[c], NAME_LENGTH);
    params[c].coeff = coeffs[c];
  }

  fft_coefficient(array, arrayLength, params, cLength, results);
  
  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (c=0; c<cLength; c++) {
    char *key, *res, *fmtstr;
    int key_len, digits, ret;
    
    if ((0 <= coeffs[c]) && (coeffs[c] < 10))
      digits = 1;
    else
      digits = floor(log10(abs(coeffs[c])));
    key_len = 14+attr_lengths[c]+digits;
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    ret = snprintf(key, key_len, "attr_%s_coeff_%d", attrs[c], coeffs[c]);
    pairs[c].key.type = jbvString;
    pairs[c].key.val.string.len = key_len;
    pairs[c].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[c].value);
    ret = snprintf(res, 32, fmtstr, results[c].value);

    pairs[c].value.type = jbvString;
    pairs[c].value.val.string.len = 32;
    pairs[c].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = cLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}
