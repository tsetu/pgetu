#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_energy_ratio_by_chunks_calc);

Datum
ts_energy_ratio_by_chunks_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: energy_ratio_by_chunks
  Input Type: energy_ratio_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength, nsLength, sfLength;
  bool *arrayNulls, *dateNulls, *nsNulls, *sfNulls;
  double *array;
  Datum *dates;
  int *num_segments, *segment_focus;
  int parse_res;

  int i;

  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_ts_energy_ratio_type(&t, &array, &arrayLength, &arrayNulls,
					 &dates, &dateLength, &dateNulls,
					 &num_segments, &nsLength, &nsNulls,
					 &segment_focus, &sfLength, &sfNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }


  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(nsLength*sizeof(JsonbPair));

  /* Iterate over the num_segments, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[nsLength];
  ERBC params[nsLength];

  for (i=0; i<nsLength; i++) {
    params[i].num_segments = num_segments[i];
    params[i].segment_focus = segment_focus[i];
  }

  if (energy_ratio_by_chunks(array, arrayLength, params, nsLength, results))
    ereport(ERROR, (errmsg("Number of segment or segment focus less than zero")));
  
  for (i=0; i<nsLength; i++) {
    char *k, *fmtstr, *res;
    int num_seg, seg_focus;
    
    k = (char *)palloc(64*sizeof(char));
    res = (char *)palloc(32*sizeof(char));
    /* Create the key */
    int ret = snprintf(k, 64, "num_segments_%d_segment_focus_%d",
		       num_segments[i], segment_focus[i]);
    pairs[i].key.type = jbvString;
    pairs[i].key.val.string.len = 64;
    pairs[i].key.val.string.val = k;

    /* Calculate the segment energy */
    create_fmtstr(&fmtstr, results[i].value);
    ret = snprintf(res, 32, fmtstr, results[i].value);     
    pfree(fmtstr);

    pairs[i].value.type = jbvString;
    pairs[i].value.val.string.len = 32;
    pairs[i].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = nsLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}

PG_FUNCTION_INFO_V1(energy_ratio_by_chunks_calc);

Datum
energy_ratio_by_chunks_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: energy_ratio_by_chunks
  Input Type: energy_ratio_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, nsLength, sfLength;
  bool *arrayNulls, *nsNulls, *sfNulls;
  double *array;
  int *num_segments, *segment_focus;
  int parse_res;

  int i;

  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_energy_ratio_type(&t, &array, &arrayLength, &arrayNulls,
				      &num_segments, &nsLength, &nsNulls,
				      &segment_focus, &sfLength, &sfNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }


  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(nsLength*sizeof(JsonbPair));

  /* Iterate over the num_segments, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[nsLength];
  ERBC params[nsLength];

  for (i=0; i<nsLength; i++) {
    params[i].num_segments = num_segments[i];
    params[i].segment_focus = segment_focus[i];
  }

  if (energy_ratio_by_chunks(array, arrayLength, params, nsLength, results))
    ereport(ERROR, (errmsg("Number of segment or segment focus less than zero")));
  
  for (i=0; i<nsLength; i++) {
    char *k, *fmtstr, *res;
    int num_seg, seg_focus;
    
    k = (char *)palloc(64*sizeof(char));
    res = (char *)palloc(32*sizeof(char));
    /* Create the key */
    int ret = snprintf(k, 64, "num_segments_%d_segment_focus_%d",
		       num_segments[i], segment_focus[i]);
    pairs[i].key.type = jbvString;
    pairs[i].key.val.string.len = 64;
    pairs[i].key.val.string.val = k;

    /* Calculate the segment energy */
    create_fmtstr(&fmtstr, results[i].value);
    ret = snprintf(res, 32, fmtstr, results[i].value);     
    pfree(fmtstr);

    pairs[i].value.type = jbvString;
    pairs[i].value.val.string.len = 32;
    pairs[i].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = nsLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}
