#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <pgtypes_numeric.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(count_above_calc);

Datum
count_above_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: count_above
  Input Type: single_float_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength;
  bool *arrayNulls, pNull;
  double *array, parameter;
  int parse_res;

  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_float_1_p_type(&t, &array, &arrayLength, &arrayNulls,
					  &parameter, &pNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(count_above(array, arrayLength, parameter));

}
