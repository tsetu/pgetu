#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_linear_trend_calc);

Datum
ts_linear_trend_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: linear_trend
  Input Type: text_date_1_p_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, dateLength, pLength;
  bool *arrayNulls, *dateNulls, *pNulls;
  double *array;
  Datum *dates;
  char **parameters;
  int *parameter_lengths;
  int parse_res;

  int c;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_text_date_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				       &dates, &dateLength, &dateNulls,
				       &parameters, &parameter_lengths,
				       &pLength, &pNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(pLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[pLength];
  LT_ATTR attrs[pLength];

  for (c=0; c<pLength; c++) {
    strncpy(attrs[c].attr, parameters[c], NAME_LENGTH);
  }
  
  linear_trend(array, arrayLength, attrs, pLength, results);
  
  for (c=0; c<pLength; c++) {
    char *key, *res, *fmtstr;
    int key_len, digits, ret;
    
    key_len = 6+parameter_lengths[c];
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    ret = snprintf(key, key_len, "attr_%s", parameters[c]);
    pairs[c].key.type = jbvString;
    pairs[c].key.val.string.len = key_len;
    pairs[c].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[c].value);
    ret = snprintf(res, 32, fmtstr, results[c].value);

    pairs[c].value.type = jbvString;
    pairs[c].value.val.string.len = 32;
    pairs[c].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = pLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}

PG_FUNCTION_INFO_V1(linear_trend_calc);

Datum
linear_trend_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: linear_trend
  Input Type: text_date_1_p_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, pLength;
  bool *arrayNulls, *pNulls;
  double *array;
  char **parameters;
  int *parameter_lengths;
  int parse_res;

  int c;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_text_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				  &parameters, &parameter_lengths,
				  &pLength, &pNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(pLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[pLength];
  LT_ATTR attrs[pLength];

  for (c=0; c<pLength; c++) {
    strncpy(attrs[c].attr, parameters[c], NAME_LENGTH);
  }
  
  linear_trend(array, arrayLength, attrs, pLength, results);
  
  for (c=0; c<pLength; c++) {
    char *key, *res, *fmtstr;
    int key_len, digits, ret;
    
    key_len = 6+parameter_lengths[c];
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    ret = snprintf(key, key_len, "attr_%s", parameters[c]);
    pairs[c].key.type = jbvString;
    pairs[c].key.val.string.len = key_len;
    pairs[c].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[c].value);
    ret = snprintf(res, 32, fmtstr, results[c].value);

    pairs[c].value.type = jbvString;
    pairs[c].value.val.string.len = 32;
    pairs[c].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = pLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}
