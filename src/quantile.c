#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <pgtypes_numeric.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(quantile_calc);

Datum
quantile_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: quantile
  Input Type: single_float_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength;
  bool *arrayNulls, pNull;
  double *array, parameter;
  int parse_res;

  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_float_1_p_type(&t, &array, &arrayLength, &arrayNulls,
					  &parameter, &pNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Check that parameter is between 0 and 1 */
  if ((parameter <0) || (parameter >1)) {
    ereport(ERROR, (errmsg("parameter must be between 0 and 1")));
  }

  PG_RETURN_FLOAT4(quantile(array, arrayLength, parameter));

}
