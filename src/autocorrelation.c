#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_autocorrelation_calc);

Datum
ts_autocorrelation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: autocorrelation
  Input Type: single_date_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength, parameter;
  bool *arrayNulls, *dateNulls, pNull;
  double *array;
  Datum *dates;
  int parse_res;

  /* Parse the __type for array, dates and parameter, 
   * return sorted by date */
  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_date_1_p_type(&t, &array, &arrayLength, &arrayNulls,
					 &dates, &dateLength, &dateNulls,
					 &parameter, &pNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* If lag is bigger than the array, return Null */
  if (arrayLength < parameter) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(autocorrelation(array, arrayLength, parameter));
}

PG_FUNCTION_INFO_V1(autocorrelation_calc);

Datum
autocorrelation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: autocorrelation
  Input Type: single_date_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, parameter;
  bool *arrayNulls, pNull;
  double *array;
  int parse_res;

  /* Parse the __type for array, dates and parameter, 
   * return sorted by date */
  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				    &parameter, &pNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* If lag is bigger than the array, return Null */
  if (arrayLength < parameter) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(autocorrelation(array, arrayLength, parameter));
}
