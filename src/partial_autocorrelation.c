#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <math.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_partial_autocorrelation_calc);

Datum
ts_partial_autocorrelation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: partial_autocorrelation
  Input Type: list_date_1_p_type
  Return Type: feature_return
  ***************************************************************************
  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, dateLength, paramLength;
  bool *arrayNulls, *dateNulls, *paramNulls;
  double *array;
  int *param;
  Datum *dates;
  int parse_res, p;

  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_list_date_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				       &dates, &dateLength, &dateNulls,
				       &param, &paramLength, &paramNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(paramLength*sizeof(JsonbPair));

  /* Iterate over the parameters, 
   * and save result in the JSONB object */
  ETU_RETURN results[paramLength];
  partial_autocorrelation(array, arrayLength, param, paramLength, results);
  
  for (p=0; p<paramLength; p++) {
    char *key, *fmtstr, *res;
    int key_len, digits, ret;
    res = (char *)palloc(32*sizeof(char));
    
    if ((0 <= param[p]) && (param[p] < 10))
      digits = 1;
    else
      digits = floor(log10(abs(param[p])));
    key_len = 5+param[p]+digits;
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    ret = snprintf(key, key_len, "lag_%d", param[p]);
    pairs[p].key.val.string.val = key;
    pairs[p].key.type = jbvString;
    pairs[p].key.val.string.len = key_len;

    /* Create the value */
    create_fmtstr(&fmtstr, results[p].value);
    ret = snprintf(res, 32, fmtstr, results[p].value);
    
    pairs[p].value.type = jbvString;
    pairs[p].value.val.string.len = 25;
    pairs[p].value.val.string.val = res;
    

  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = paramLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));

}

PG_FUNCTION_INFO_V1(partial_autocorrelation_calc);

Datum
partial_autocorrelation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: partial_autocorrelation
  Input Type: list_date_1_p_type
  Return Type: feature_return
  ***************************************************************************
  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, paramLength;
  bool *arrayNulls, *paramNulls;
  double *array;
  int *param;
  int parse_res, p;

  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_list_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				  &param, &paramLength, &paramNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(paramLength*sizeof(JsonbPair));

  /* Iterate over the parameters, 
   * and save result in the JSONB object */
  ETU_RETURN results[paramLength];
  partial_autocorrelation(array, arrayLength, param, paramLength, results);
  
  for (p=0; p<paramLength; p++) {
    char *key, *fmtstr, *res;
    int key_len, digits, ret;
    res = (char *)palloc(32*sizeof(char));
    
    if ((0 <= param[p]) && (param[p] < 10))
      digits = 1;
    else
      digits = floor(log10(abs(param[p])));
    key_len = 5+param[p]+digits;
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    ret = snprintf(key, key_len, "lag_%d", param[p]);
    pairs[p].key.val.string.val = key;
    pairs[p].key.type = jbvString;
    pairs[p].key.val.string.len = key_len;

    /* Create the value */
    create_fmtstr(&fmtstr, results[p].value);
    ret = snprintf(res, 32, fmtstr, results[p].value);
    
    pairs[p].value.type = jbvString;
    pairs[p].value.val.string.len = 25;
    pairs[p].value.val.string.val = res;
    

  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = paramLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));

}
