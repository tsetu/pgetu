#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_approximate_entropy_calc);

Datum
ts_approximate_entropy_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: approximate_entropy
  Input Type: approx_entropy_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, dateLength;
  bool *arrayNulls, *dateNulls, mNull, rNull;
  double *array, r;
  Datum *dates;
  int parse_res;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_ts_approx_entropy_type(&t, &array,
					   &arrayLength, &arrayNulls,
					   &dates, &dateLength, &dateNulls,
					   &m, &mNull, &r, &rNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Check that r is greater than zero and m is less than the array length */
  if (r <= 0)
    PG_RETURN_NULL();

  if (arrayLength < m+1)
    PG_RETURN_FLOAT4(0.0);

  /* Calculate approximate entropy */
  PG_RETURN_FLOAT4(approximate_entropy(array, arrayLength, m, r));
}

PG_FUNCTION_INFO_V1(approximate_entropy_calc);

Datum
approximate_entropy_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: approximate_entropy
  Input Type: approx_entropy_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength;
  bool *arrayNulls, mNull, rNull;
  double *array, r;
  int parse_res;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_approx_entropy_type(&t, &array,
					&arrayLength, &arrayNulls,
					&m, &mNull, &r, &rNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Check that r is greater than zero and m is less than the array length */
  if (r <= 0)
    PG_RETURN_NULL();

  if (arrayLength < m+1)
    PG_RETURN_FLOAT4(0.0);

  /* Calculate approximate entropy */
  PG_RETURN_FLOAT4(approximate_entropy(array, arrayLength, m, r));
}
