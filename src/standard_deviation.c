#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <utils/array.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(standard_deviation_calc);

Datum
standard_deviation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: standard_deviation
  Input Type: REAL[]
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  ArrayType *a = PG_GETARG_ARRAYTYPE_P(0);
  int arrayLength;
  bool *arrayNulls;
  double *array;
  int parse_res, i;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_postgres_double_array(a, &array, &arrayLength, &arrayNulls);

  if (!parse_res) {
     PG_RETURN_NULL();
  }

  /* Calculate the number of observations over the standard_deviation */
  PG_RETURN_FLOAT4(standard_deviation(array, arrayLength));
}
