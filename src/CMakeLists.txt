cmake_minimum_required(VERSION 3.21)

project(pgetu VERSION 0.9.0)

# Do we have etu installed
find_library(ETU etu REQUIRED)

# do we have the Intel oneAPI C compiler
SET(ICX,find_program(ICX icx))
if(ICX STREQUAL "ICX-NOTFOUND")
  message("Using compiler ${CMAKE_C_COMPILER_ID}")
  set(USE_INTEL OFF)
else()
  set(USE_INTEL ON CACHE BOOL 1)
  set(CMAKE_C_COMPILER icx)
  message("Using compiler icx")
endif()

find_program(PG_CONFIG pg_config)
#  PATHS ${PostgreSQL_ROOT_DIRECTORIES}
#PATH_SUFFIXES bin)

if(NOT PG_CONFIG)
  message(FATAL_ERROR "pg_config not found!")
endif()

macro(create_var VAR OPT)
  execute_process(COMMAND ${PG_CONFIG} ${OPT}
    OUTPUT_VARIABLE ${VAR}
    OUTPUT_STRIP_TRAILING_WHITESPACE)
endmacro()

create_var(PGINCLUDEDIR --includedir)
create_var(PGINCLUDEDIRSRV --includedir-server)

configure_file(pgetu_cmake.h.in pgetu_cmake.h)
include_directories(${CMAKE_CURRENT_BINARY_DIR})

file(GLOB pgetu_srcs *.c)

add_library(pgetu SHARED ${pgetu_srcs})
target_link_libraries(pgetu PRIVATE pgtypes etu)
target_include_directories(pgetu SYSTEM PRIVATE ${PGINCLUDEDIR} ${PGINCLUDEDIRSRV})

install(TARGETS pgetu DESTINATION ${CMAKE_INSTALL_LIBDIR})
