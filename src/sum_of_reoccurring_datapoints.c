#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(sum_of_reoccurring_datapoints_calc);

Datum
sum_of_reoccurring_datapoints_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: sum_of_reoccurring_datapoints
  Input Type: REAL[]
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  ArrayType *a = PG_GETARG_ARRAYTYPE_P(0);
  int arrayLength;
  bool *arrayNulls;
  double *array;
  int parse_res;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_postgres_double_array(a, &array, &arrayLength, &arrayNulls);

  if (!parse_res) {
     PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(sum_of_reoccurring_datapoints(array, arrayLength));
}
