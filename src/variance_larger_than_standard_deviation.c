#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(variance_larger_than_standard_deviation_calc);

Datum
variance_larger_than_standard_deviation_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: variance_larger_than_standard_deviation
  Input Type: REAL[]
  Return Type: BOOLEAN
  ***************************************************************************

  **************************************************************************/
  ArrayType *a = PG_GETARG_ARRAYTYPE_P(0);
  int arrayLength;
  bool *arrayNulls;
  double *array;
  int parse_res;
  double var, sd;

  /* Parse the __type for array and dates, return sorted by date */
  parse_res = parse_postgres_double_array(a, &array, &arrayLength, &arrayNulls);

  if (!parse_res) {
     PG_RETURN_NULL();
  }

  PG_RETURN_BOOL((bool)variance_larger_than_standard_deviation(array, arrayLength));
}
