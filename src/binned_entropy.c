#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(binned_entropy_calc);

Datum
binned_entropy_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: binned_entropy
  Input Type: single_1_p_type
  Return Type: REAL
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int arrayLength, parameter;
  bool *arrayNulls, pNull;
  double *array;
  int parse_res;

  /* Parse the single_1_p_type for array and parameter*/
  parse_res = parse_single_1_p_type(&t, &array, &arrayLength, &arrayNulls,
				    &parameter, &pNull);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  PG_RETURN_FLOAT4(binned_entropy(array, arrayLength, parameter));
}

