#include <postgres.h>
#include <executor/executor.h>
#include <utils/array.h>

int parse_postgres_double_array(ArrayType *a, double **array,
				int *arrayLength, bool **arrayNulls);

int parse_value_date_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  Datum **dates, int *dateLength,
			  bool **dateNulls);

int parse_single_1_p_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  int *parameter, bool *pNull);

int parse_single_date_1_p_type(HeapTupleHeader *t, double **list,
			       int *listLength, bool **listNulls,
			       Datum **dates, int *dateLength,
			       bool **dateNulls,
			       int *parameter, bool *pNull);

int parse_float_1_p_type(HeapTupleHeader *t, double **list,
			 int *listLength, bool **listNulls,
			 double **param, int *paramLength,
			 bool **paramNulls);

int parse_float_date_1_p_type(HeapTupleHeader *t, double **list,
			      int *listLength, bool **listNulls,
			      Datum **dates, int *dateLength,
			      bool **dateNulls,
			      double **param, int *paramLength,
			      bool **paramNulls);

int parse_double_float_1_p_type(HeapTupleHeader *t, double **list,
				int *listLength, bool **listNulls,
				double *parameter1, bool *p1Null,
				double *parameter2, bool *p2Null);

int parse_single_float_1_p_type(HeapTupleHeader *t, double **list,
				int *listLength, bool **listNulls,
				double *parameter, bool *pNull);

int parse_single_date_float_1_p_type(HeapTupleHeader *t, double **list,
				     int *listLength, bool **listNulls,
				     Datum **dates, int *dateLength,
				     bool **dateNulls,
				     double *parameter, bool *pNull);

int parse_ts_energy_ratio_type(HeapTupleHeader *t, double **list,
			       int *listLength, bool **listNulls,
			       Datum **dates, int *dateLength,
			       bool **dateNulls,
			       int **num_segments, int *nsLength,
			       bool **nsNulls,
			       int **segment_focus, int *sfLength,
			       bool **sfNulls);

int parse_energy_ratio_type(HeapTupleHeader *t, double **list,
			    int *listLength, bool **listNulls,
			    int **num_segments, int *nsLength,
			    bool **nsNulls,
			    int **segment_focus, int *sfLength,
			    bool **sfNulls);

int parse_ts_approx_entropy_type(HeapTupleHeader *t, double **list,
			      int *listLength, bool **listNulls,
			      Datum **dates, int *dateLength,
			      bool **dateNulls,
			      int *m, bool *mNull,
			      double *r, bool *rNull);

int parse_approx_entropy_type(HeapTupleHeader *t, double **list,
			      int *listLength, bool **listNulls,
			      int *m, bool *mNull,
			      double *r, bool *rNull);

int parse_ts_agg_autocorrelation_type(HeapTupleHeader *t, double **list,
				   int *listLength, bool **listNulls,
				   Datum **dates, int *dateLength,
				   bool **dateNulls,
				   int **maxlags, int *mLength,
				   bool **mNulls,
				   char ***f_aggs, int **f_aggs_length,
				   int *fLength, bool **fNulls);

int parse_agg_autocorrelation_type(HeapTupleHeader *t, double **list,
				   int *listLength, bool **listNulls,
				   int **maxlags, int *mLength,
				   bool **mNulls,
				   char ***f_aggs, int **f_aggs_length,
				   int *fLength, bool **fNulls);

int parse_ts_agg_dickey_type(HeapTupleHeader *t, double **list,
			     int *listLength, bool **listNulls,
			     Datum **dates, int *dateLength,
			     bool **dateNulls,
			     char ***attrs, int **attr_lengths,
			     int *aLength, bool **aNulls,
			     char ***autolags, int **autolag_lengths,
			     int *alLength, bool **alNulls);

int parse_agg_dickey_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  char ***attrs, int **attr_lengths,
			  int *aLength, bool **aNulls,
			  char ***autolags, int **autolag_lengths,
			  int *alLength, bool **alNulls);

int parse_ts_fft_coef_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			Datum **dates, int *dateLength,
			bool **dateNulls,
			int **coeffs, int *cLength,
			bool **cNulls,
			char ***attrs, int **attr_length,
			int *aLength, bool **aNulls);

int parse_fft_coef_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			int **coeffs, int *cLength,
			bool **cNulls,
			char ***attrs, int **attr_length,
			int *aLength, bool **aNulls);

int parse_ts_fft_agg_type(HeapTupleHeader *t, double **list,
		       int *listLength, bool **listNulls,
		       Datum **dates, int *dateLength,
		       bool **dateNulls,
		       char ***atypes, int **atype_length,
		       int *aLength, bool **aNulls);

int parse_fft_agg_type(HeapTupleHeader *t, double **list,
		       int *listLength, bool **listNulls,
		       char ***atypes, int **atype_length,
		       int *aLength, bool **aNulls);

int parse_text_date_1_p_type(HeapTupleHeader *t, double **list,
			     int *listLength, bool **listNulls,
			     Datum **dates, int *dateLength,
			     bool **dateNulls,
			     char ***parameters, int **parameter_length,
			     int *pLength, bool **pNulls);

int parse_text_1_p_type(HeapTupleHeader *t, double **list,
		       int *listLength, bool **listNulls,
		       char ***parameters, int **parameter_length,
		       int *pLength, bool **pNulls);

int parse_list_1_p_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			int **param, int *paramLength,
			bool **paramNulls);

int parse_list_date_1_p_type(HeapTupleHeader *t, double **list,
			     int *listLength, bool **listNulls,
			     Datum **dates, int *dateLength,
			     bool **dateNulls,
			     int **param, int *paramLength,
			     bool **paramNulls);

int doublesortcmp(const void *a, const void *b);

void create_fmtstr(char **fmtstr, double value);
