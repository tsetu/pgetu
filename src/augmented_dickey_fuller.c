#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_augmented_dickey_fuller_calc);

Datum
ts_augmented_dickey_fuller_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: augmented_dickey_fuller
  Input Type: agg_dickey_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, dateLength, aLength, alLength;
  bool *arrayNulls, *dateNulls, *aNulls, *alNulls;
  double *array;
  Datum *dates;
  char **attrs, **autolags;
  int *attr_lengths, *autolag_lengths;
  int parse_res;

  int a;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_ts_agg_dickey_type(&t, &array, &arrayLength, &arrayNulls,
				    &dates, &dateLength, &dateNulls,
				    &attrs, &attr_lengths,
				    &aLength, &aNulls,
				    &autolags, &autolag_lengths,
				    &alLength, &alNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(aLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[aLength];
  AUG_DF params[aLength];

  for (a=0; a<aLength; a++) {
    strncpy(params[a].attr, attrs[a], NAME_LENGTH);
    strncpy(params[a].autolag, autolags[a], NAME_LENGTH);
  }

  augmented_dickey_fuller(array, arrayLength, params, aLength, results);
  
  for (a=0; a<aLength; a++) {
    char *key, *res, *fmtstr;
    int key_len = 15+attr_lengths[a]+autolag_lengths[a];
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    int ret = snprintf(key, key_len, "attr_%s_autolag_%s",
		       attrs[a], autolags[a]);
    pairs[a].key.type = jbvString;
    pairs[a].key.val.string.len = key_len;
    pairs[a].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    ret = snprintf(res, 32, "None");

    pairs[a].value.type = jbvString;
    pairs[a].value.val.string.len = 32;
    pairs[a].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = aLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}

PG_FUNCTION_INFO_V1(augmented_dickey_fuller_calc);

Datum
augmented_dickey_fuller_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: augmented_dickey_fuller
  Input Type: agg_dickey_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, aLength, alLength;
  bool *arrayNulls, *aNulls, *alNulls;
  double *array;
  char **attrs, **autolags;
  int *attr_lengths, *autolag_lengths;
  int parse_res;

  int a;
  
  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_agg_dickey_type(&t, &array, &arrayLength, &arrayNulls,
				    &attrs, &attr_lengths,
				    &aLength, &aNulls,
				    &autolags, &autolag_lengths,
				    &alLength, &alNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(aLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[aLength];
  AUG_DF params[aLength];

  for (a=0; a<aLength; a++) {
    strncpy(params[a].attr, attrs[a], NAME_LENGTH);
    strncpy(params[a].autolag, autolags[a], NAME_LENGTH);
  }

  augmented_dickey_fuller(array, arrayLength, params, aLength, results);
  
  for (a=0; a<aLength; a++) {
    char *key, *res, *fmtstr;
    int key_len = 15+attr_lengths[a]+autolag_lengths[a];
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    int ret = snprintf(key, key_len, "attr_%s_autolag_%s",
		       attrs[a], autolags[a]);
    pairs[a].key.type = jbvString;
    pairs[a].key.val.string.len = key_len;
    pairs[a].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    ret = snprintf(res, 32, "None");

    pairs[a].value.type = jbvString;
    pairs[a].value.val.string.len = 32;
    pairs[a].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = aLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}
