#include <string.h>
#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <datatype/timestamp.h>
#include <utils/array.h>
#include <utils/builtins.h>
#include <utils/datum.h>
#include <utils/lsyscache.h>
#include <utils/timestamp.h>
#include <pgtypes_timestamp.h>
#include <math.h>

PG_MODULE_MAGIC;

struct datetosort {
  Timestamp date;
  int index;
};

int datetosortcmp(const void *a, const void *b) {
  /*********************************************************************
   * Function qsort uses to compare to timestamps and sort 
   * in increasing order.
   ********************************************************************/
  struct datetosort *a1 = (struct datetosort *)a;
  struct datetosort *a2 = (struct datetosort *)b;

  if ((*a1).date > (*a2).date)
    return 1;
  else if ((*a1).date < (*a2).date)
    return -1;
  else
    return 0;
}

int doublesortcmp(const void *a, const void *b) {
  if (*(double*)a > *(double*)b)
    return 1;
  else if (*(double*)a < *(double*)b)
    return -1;
  else
    return 0;
}

int extract_attribute(HeapTupleHeader *t, char *attributeName,
		      int *attributeLength,
		      bool **attributeNulls, Datum **attributeContent) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres, extract the element labeled 
   * by attributeName and return the contents in attributeContent.
   ********************************************************************/
  Datum attribute;
  bool attributeisnull;
    
  ArrayType *attributeArray;
  Oid elemTypeId;
  int16 elemTypeWidth;
  bool elemTypeByValue;
  char elemTypeAlignmentCode;
  bool *attNulls;
  Datum *attContent;

  /* Get just the attribute labeled by attributeName from the argument */
  attribute = GetAttributeByName(*t, attributeName, &attributeisnull);
  if (attributeisnull)
    return 0;

  /* Next extract information about the data type of the attribute */
  attributeArray = DatumGetArrayTypeP(attribute);
  elemTypeId = ARR_ELEMTYPE(attributeArray);
  get_typlenbyvalalign(elemTypeId, &elemTypeWidth, &elemTypeByValue, &elemTypeAlignmentCode);

  /* Using the data type, extract the actual content of the argument */
  deconstruct_array(attributeArray, elemTypeId, elemTypeWidth, elemTypeByValue, elemTypeAlignmentCode, &attContent, &attNulls, attributeLength);

  /* Save the content and array identifying if the corresponding
   * element is Null */
  *attributeContent = attContent;
  *attributeNulls = attNulls;
  return 1;
}

int get_postgres_array(HeapTupleHeader *t, char *attribute,
		       Datum **attrContent,
		       int *attrLength, bool **attrNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres, extract the element labeled 
   * by attributeName and return the array
   ********************************************************************/
  int returnValue;
  bool *aNulls;

  /* Get the postgres array for attribute */
  returnValue = extract_attribute(t, attribute,
				  attrLength, &aNulls, attrContent);
  if (!returnValue)
    return 0;
  *attrNulls = aNulls;

  return 1;
}

int get_double_array(HeapTupleHeader *t, char *attribute,
		     double **array,
		     int *attrLength, bool **attrNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres, extract the element labeled 
   * by attributeName and return the array as double
   ********************************************************************/
  Datum *attrContent;
  int returnValue, i;
  /* Get the double array attribute  */
  returnValue = get_postgres_array(t, attribute,
				   &attrContent, attrLength, attrNulls);
  
  if (!returnValue)
    return 0;

  /* Create the array for list that will be returned for 
   * processing. */
  *array = (double*)palloc(*attrLength*sizeof(double));
  for (i = 0; i< *attrLength; i++) {
    (*array)[i] = (double) DatumGetFloat4(attrContent[i]);
  }

  return 1;
}

int get_int_array(HeapTupleHeader *t, char *attribute,
		  int **array,
		  int *attrLength, bool **attrNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres, extract the element labeled 
   * by attributeName and return the array as int
   ********************************************************************/
  Datum *attrContent;
  int returnValue, i;
  /* Get the int array attribute  */
  returnValue = get_postgres_array(t, attribute,
				   &attrContent, attrLength, attrNulls);
  
  if (!returnValue)
    return 0;

  /* Create the array for list that will be returned for 
   * processing. */
  *array = (int*)palloc(*attrLength*sizeof(int));
  for (i = 0; i< *attrLength; i++) {
    (*array)[i] = DatumGetInt32(attrContent[i]);
  }

  return 1;
}

int get_text_array(HeapTupleHeader *t, char *attribute,
		   char ***array, int **array_length,
		   int *attrLength, bool **attrNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres, extract the element labeled 
   * by attributeName and return the array as int
   ********************************************************************/
  Datum *attrContent;
  int returnValue, i;
  /* Get the int array attribute  */
  returnValue = get_postgres_array(t, attribute,
				   &attrContent, attrLength, attrNulls);
  
  if (!returnValue)
    return 0;

  /* Create the array for list that will be returned for 
   * processing. */
  *array = (char**)palloc(*attrLength*sizeof(char *));
  *array_length = (int*)palloc(*attrLength*sizeof(int));
  for (i = 0; i< *attrLength; i++) {
    text *element = DatumGetTextP(attrContent[i]);
    //(*array)[i] = VARDATA(element);
    (*array)[i] = TextDatumGetCString(attrContent[i]);
    (*array_length)[i] = VARSIZE(element) - VARHDRSZ;
  }

  return 1;
}

int get_postgres_int(HeapTupleHeader *t, char *attribute,
		     int *parameter, bool *pNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres that will extract the integer 
   * parameter with name attribute
   ********************************************************************/
  int returnValue;
  Datum pContent;

    /* Get the parameter "parameter" */
  pContent = GetAttributeByName(*t, attribute, pNull);

  /* Convert to parameter to integer */
  if (!*pNull) {
    *parameter = DatumGetInt32(pContent);
  } else {
    return 0;
  }

  return 1;
}

int get_postgres_double(HeapTupleHeader *t, char *attribute,
			double *parameter, bool *pNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres that will extract the double 
   * parameter with name attribute
   ********************************************************************/
  int returnValue;
  Datum pContent;

    /* Get the parameter "parameter" */
  pContent = GetAttributeByName(*t, attribute, pNull);

  /* Convert to parameter to integer */
  if (!*pNull) {
    *parameter = (double)DatumGetFloat4(pContent);
  } else {
    return 0;
  }

  return 1;
}

int get_double_array_date(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  Datum **dates, int *dateLength,
			  bool **dateNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres 
   * extract the array "list" containing the values to process
   * the array "dates" containing the corresponding timestamp
   * for each value.
   * 
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue, i;
  Datum *listContent, *dateContent;
  struct datetosort *ds;

  /* Get the double array "list" */
  returnValue = get_postgres_array(t, "list",
				   &listContent, listLength, listNulls);
  if (!returnValue)
    return 0;


  /* Get the Timestamp array "dates" */
  returnValue = get_postgres_array(t, "dates",
				   &dateContent, dateLength, dateNulls);
  if (!returnValue)
    return 0;

  /* If the number of elements of list and date don't match,
   * something has gone seriously wrong. */
  if (*listLength != *dateLength) {
    ereport(ERROR, (errmsg("mismatched list and date vector lengths")));
  }

  /* Create the arrays for list and dates that will be returned for 
   * processing. */
  *list = (double*)palloc(*listLength*sizeof(double));
  *dates = (Datum*)palloc(*dateLength*sizeof(Datum));
  /* Create an array that will be used to perform an argsort
   * of the dates array. */
  ds = (struct datetosort*)palloc(*dateLength*sizeof(struct datetosort));

  for (i=0; i < *listLength; i++) {
    ds[i].date = DatumGetTimestamp(dateContent[i]);
    ds[i].index = i;
  }

  /* Use qsort to argsort the dates */
  qsort(ds, *dateLength, sizeof(ds[0]), datetosortcmp);

  /* Populate both the list and dates array for return
   * This will use the argsort result to return the values
   * in increasing date order */
  for (i = 0; i< *listLength; i++) {
    (*list)[i] = (double) DatumGetFloat4(listContent[ds[i].index]);
    (*dates)[i] = dateContent[ds[i].index];
  }

  return 1;
}

int parse_postgres_double_array(ArrayType *a, double **array,
				int *arrayLength, bool **arrayNulls) {
  /*********************************************************************
   * Helper function given a postgres array of reals, returns a 
   * c array of doubles along with the length.
   ********************************************************************/
  int i;
  Oid elemTypeId;
  int16 elemTypeWidth;
  bool elemTypeByValue;
  char elemTypeAlignmentCode;
  bool *arrNulls;
  Datum *arrContent;

  elemTypeId = ARR_ELEMTYPE(a);
  get_typlenbyvalalign(elemTypeId, &elemTypeWidth, &elemTypeByValue, &elemTypeAlignmentCode);

  /* Using the data type, extract the actual content of the argument */
  deconstruct_array(a, elemTypeId, elemTypeWidth, elemTypeByValue, elemTypeAlignmentCode, &arrContent, &arrNulls, arrayLength);

  /* Save the array identifying if the corresponding
   * element is Null */
  *arrayNulls = arrNulls;

  /* Create the array that will be returned for 
   * processing. */
  *array = (double*)palloc(*arrayLength*sizeof(double));
  for (i = 0; i< *arrayLength; i++) {
    (*array)[i] = (double) DatumGetFloat4(arrContent[i]);
  }

  return 1;
}

int parse_value_date_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  Datum **dates, int *dateLength,
			  bool **dateNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a value_date_type, 
   * extract the array "list" containing the values to process
   * the array "dates" containing the corresponding timestamp
   * for each value.
   * 
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_single_1_p_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  int *parameter, bool *pNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a single_1_p_type, 
   * extract the array "list" containing the values to process
   * and the parameter
   * 
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);
  if (!returnValue)
    return 0;

  /* Get the parameter "parameter" */
  returnValue = get_postgres_int(t, "parameter",
				 parameter, pNull);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_single_date_1_p_type(HeapTupleHeader *t, double **list,
			       int *listLength, bool **listNulls,
			       Datum **dates, int *dateLength,
			       bool **dateNulls,
			       int *parameter, bool *pNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a single_date_1_p_type, 
   * extract the array "list" containing the values to process
   * the array "dates" containing the corresponding timestamp
   * for each value.
   * 
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get the postgres datum for the parameter */
  returnValue = get_postgres_int(t, "parameter",
				 parameter, pNull);

  if (!returnValue)
    return 0;
  return 1;
}

int parse_float_1_p_type(HeapTupleHeader *t, double **list,
			 int *listLength, bool **listNulls,
			 double **param, int *paramLength,
			 bool **paramNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a value_date_type, 
   * extract the array "list" containing the values to process
   * the array "param" containing the parameters for the function
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);
  if (!returnValue)
    return 0;

  /* Get the postgres array for "parameter" */
  returnValue = get_double_array(t, "parameter",
				 param, paramLength, paramNulls);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_float_date_1_p_type(HeapTupleHeader *t, double **list,
			      int *listLength, bool **listNulls,
			      Datum **dates, int *dateLength,
			      bool **dateNulls,
			      double **param, int *paramLength,
			      bool **paramNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a value_date_type, 
   * extract the array "list" containing the values to process
   * the array "param" containing the parameters for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get the postgres array for "parameter" */
  returnValue = get_double_array(t, "parameter",
				 param, paramLength, paramNulls);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_single_float_1_p_type(HeapTupleHeader *t, double **list,
				int *listLength, bool **listNulls,
				double *parameter, bool *pNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a single_1_p_type, 
   * extract the array "list" containing the values to process
   * and the parameter
   * 
   ********************************************************************/
  int returnValue, i;
  Datum *listContent, pContent;
  bool *lNulls;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;

  /* Get the postgres datum for the parameter */
  returnValue = get_postgres_double(t, "parameter",
				    parameter, pNull);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_double_float_1_p_type(HeapTupleHeader *t, double **list,
				int *listLength, bool **listNulls,
				double *parameter1, bool *p1Null,
				double *parameter2, bool *p2Null) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a single_1_p_type, 
   * extract the array "list" containing the values to process
   * and the parameter
   * 
   ********************************************************************/
  int returnValue, i;
  Datum *listContent, pContent;
  bool *lNulls;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;

  /* Get the postgres datum for the parameter1 */
  returnValue = get_postgres_double(t, "parameter1",
				    parameter1, p1Null);

  if (!returnValue)
    return 0;

  /* Get the postgres datum for the parameter2 */
  returnValue = get_postgres_double(t, "parameter2",
				    parameter2, p2Null);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_single_date_float_1_p_type(HeapTupleHeader *t, double **list,
				     int *listLength, bool **listNulls,
				     Datum **dates, int *dateLength,
				     bool **dateNulls,
				     double *parameter, bool *pNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a single_date_1_p_type, 
   * extract the array "list" containing the values to process
   * the array "dates" containing the corresponding timestamp
   * for each value.
   * 
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get the postgres datum for the parameter */
  returnValue = get_postgres_double(t, "parameter",
				    parameter, pNull);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_ts_energy_ratio_type(HeapTupleHeader *t, double **list,
			       int *listLength, bool **listNulls,
			       Datum **dates, int *dateLength,
			       bool **dateNulls,
			       int **num_segments, int *nsLength,
			       bool **nsNulls,
			       int **segment_focus, int *sfLength,
			       bool **sfNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a energy_ratio_type, 
   * extract the array "list" containing the values to process
   * the array "num_segments" containing the num_segments and 
   * "segment_focus" containing the segment_focus for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get the postgres array for "num_segments" */
  returnValue = get_int_array(t, "num_segments",
			      num_segments, nsLength, nsNulls);
  if (!returnValue)
    return 0;

  /* Get the postgres array for "segment_focus" */
  returnValue = get_int_array(t, "segment_focus",
			      segment_focus, sfLength, sfNulls);
  if (!returnValue)
    return 0;

  /* If the number of elements of num_segments and segment_focus,
   * there is a problem. */
  if (*nsLength != *sfLength) {
    ereport(ERROR, (errmsg("Number of segments and segment focus must be the same length.")));
  }

  return 1;
}

int parse_energy_ratio_type(HeapTupleHeader *t, double **list,
			    int *listLength, bool **listNulls,
			    int **num_segments, int *nsLength,
			    bool **nsNulls,
			    int **segment_focus, int *sfLength,
			    bool **sfNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a energy_ratio_type, 
   * extract the array "list" containing the values to process
   * the array "num_segments" containing the num_segments and 
   * "segment_focus" containing the segment_focus for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get the postgres array for "num_segments" */
  returnValue = get_int_array(t, "num_segments",
			      num_segments, nsLength, nsNulls);
  if (!returnValue)
    return 0;

  /* Get the postgres array for "segment_focus" */
  returnValue = get_int_array(t, "segment_focus",
			      segment_focus, sfLength, sfNulls);
  if (!returnValue)
    return 0;

  /* If the number of elements of num_segments and segment_focus,
   * there is a problem. */
  if (*nsLength != *sfLength) {
    ereport(ERROR, (errmsg("Number of segments and segment focus must be the same length.")));
  }

  return 1;
}

int parse_ts_approx_entropy_type(HeapTupleHeader *t, double **list,
				 int *listLength, bool **listNulls,
				 Datum **dates, int *dateLength,
				 bool **dateNulls,
				 int *m, bool *mNull,
				 double *r, bool *rNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter "m" and "r" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get "m" */
  returnValue = get_postgres_int(t, "m", m, mNull);
  if (!returnValue)
    return 0;

  /* Get "r" */
  returnValue = get_postgres_double(t, "r", r, rNull);
  if (!returnValue)
    return 0;

  return 1;
}

int parse_approx_entropy_type(HeapTupleHeader *t, double **list,
			      int *listLength, bool **listNulls,
			      int *m, bool *mNull,
			      double *r, bool *rNull) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter "m" and "r" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get "m" */
  returnValue = get_postgres_int(t, "m", m, mNull);
  if (!returnValue)
    return 0;

  /* Get "r" */
  returnValue = get_postgres_double(t, "r", r, rNull);
  if (!returnValue)
    return 0;

  return 1;
}

int parse_ts_agg_autocorrelation_type(HeapTupleHeader *t, double **list,
				      int *listLength, bool **listNulls,
				      Datum **dates, int *dateLength,
				      bool **dateNulls,
				      int **maxlags, int *mLength,
				      bool **mNulls,
				      char ***f_aggs, int **f_aggs_length,
				      int *fLength, bool **fNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "f_agg" and "maxlag" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get "maxlag" */
  returnValue = get_int_array(t, "maxlag", maxlags, mLength, mNulls);
  if (!returnValue)
    return 0;
  
  /* Get "f_agg" */
  returnValue = get_text_array(t, "f_agg", f_aggs, f_aggs_length,
			       fLength, fNulls);
  if (!returnValue)
    return 0;
  

  /* If the number of elements of maxLag and f_agg do not agree,
   * there is a problem */
  if (*mLength != *fLength) {
    ereport(ERROR, (errmsg("Number of maxLags and f_aggs must be the same.")));
  }
  return 1;
}

int parse_agg_autocorrelation_type(HeapTupleHeader *t, double **list,
				   int *listLength, bool **listNulls,
				   int **maxlags, int *mLength,
				   bool **mNulls,
				   char ***f_aggs, int **f_aggs_length,
				   int *fLength, bool **fNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "f_agg" and "maxlag" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get "maxlag" */
  returnValue = get_int_array(t, "maxlag", maxlags, mLength, mNulls);
  if (!returnValue)
    return 0;
  
  /* Get "f_agg" */
  returnValue = get_text_array(t, "f_agg", f_aggs, f_aggs_length,
			       fLength, fNulls);
  if (!returnValue)
    return 0;
  

  /* If the number of elements of maxLag and f_agg do not agree,
   * there is a problem */
  if (*mLength != *fLength) {
    ereport(ERROR, (errmsg("Number of maxLags and f_aggs must be the same.")));
  }
  return 1;
}

int parse_ts_agg_dickey_type(HeapTupleHeader *t, double **list,
			     int *listLength, bool **listNulls,
			     Datum **dates, int *dateLength,
			     bool **dateNulls,
			     char ***attrs, int **attr_lengths,
			     int *aLength, bool **aNulls,
			     char ***autolags, int **autolag_lengths,
			     int *alLength, bool **alNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "attr" and "autolag" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get "attr" */
  returnValue = get_text_array(t, "attr", attrs, attr_lengths,
			       aLength, aNulls);
  if (!returnValue)
    return 0;
  
  /* Get "autolag" */
  returnValue = get_text_array(t, "autolag", autolags, autolag_lengths,
			       alLength, alNulls);
  if (!returnValue)
    return 0;
  

  /* If the number of elements of autolag and attr do not agree,
   * there is a problem */
  if (*aLength != *alLength) {
    ereport(ERROR, (errmsg("Number of attrs and autolagss must be the same.")));
  }
  return 1;
}

int parse_agg_dickey_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  char ***attrs, int **attr_lengths,
			  int *aLength, bool **aNulls,
			  char ***autolags, int **autolag_lengths,
			  int *alLength, bool **alNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "attr" and "autolag" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get "attr" */
  returnValue = get_text_array(t, "attr", attrs, attr_lengths,
			       aLength, aNulls);
  if (!returnValue)
    return 0;
  
  /* Get "autolag" */
  returnValue = get_text_array(t, "autolag", autolags, autolag_lengths,
			       alLength, alNulls);
  if (!returnValue)
    return 0;
  

  /* If the number of elements of autolag and attr do not agree,
   * there is a problem */
  if (*aLength != *alLength) {
    ereport(ERROR, (errmsg("Number of attrs and autolagss must be the same.")));
  }
  return 1;
}

int parse_ts_fft_coef_type(HeapTupleHeader *t, double **list,
			   int *listLength, bool **listNulls,
			   Datum **dates, int *dateLength,
			   bool **dateNulls,
			   int **coeffs, int *cLength,
			   bool **cNulls,
			   char ***attrs, int **attr_length,
			   int *aLength, bool **aNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "coeff" and "attr" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get "coeff" */
  returnValue = get_int_array(t, "coeff", coeffs, cLength, cNulls);
  if (!returnValue)
    return 0;
  
  /* Get "f_agg" */
  returnValue = get_text_array(t, "attr", attrs, attr_length,
			       aLength, aNulls);
  if (!returnValue)
    return 0;
  

  /* If the number of elements of coeff and attr do not agree,
   * there is a problem */
  if (*cLength != *aLength) {
    ereport(ERROR, (errmsg("Number of coeff and attr must be the same.")));
  }
  return 1;
}

int parse_fft_coef_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			int **coeffs, int *cLength,
			bool **cNulls,
			char ***attrs, int **attr_length,
			int *aLength, bool **aNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "coeff" and "attr" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get "coeff" */
  returnValue = get_int_array(t, "coeff", coeffs, cLength, cNulls);
  if (!returnValue)
    return 0;
  
  /* Get "f_agg" */
  returnValue = get_text_array(t, "attr", attrs, attr_length,
			       aLength, aNulls);
  if (!returnValue)
    return 0;
  

  /* If the number of elements of coeff and attr do not agree,
   * there is a problem */
  if (*cLength != *aLength) {
    ereport(ERROR, (errmsg("Number of coeff and attr must be the same.")));
  }
  return 1;
}

int parse_ts_fft_agg_type(HeapTupleHeader *t, double **list,
			  int *listLength, bool **listNulls,
			  Datum **dates, int *dateLength,
			  bool **dateNulls,
			  char ***atypes, int **atype_length,
			  int *aLength, bool **aNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "aggtype" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get "aggtype" */
  returnValue = get_text_array(t, "aggtype", atypes, atype_length,
			       aLength, aNulls);
  if (!returnValue)
    return 0;
  
  return 1;
}

int parse_fft_agg_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			char ***atypes, int **atype_length,
			int *aLength, bool **aNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "aggtype" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get "aggtype" */
  returnValue = get_text_array(t, "aggtype", atypes, atype_length,
			       aLength, aNulls);
  if (!returnValue)
    return 0;
  
  return 1;
}

int parse_text_date_1_p_type(HeapTupleHeader *t, double **list,
			     int *listLength, bool **listNulls,
			     Datum **dates, int *dateLength,
			     bool **dateNulls,
			     char ***parameters, int **parameter_length,
			     int *pLength, bool **pNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "aggtype" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);

  if (!returnValue)
    return 0;
    
  /* Get "aggtype" */
  returnValue = get_text_array(t, "parameter", parameters, parameter_length,
			       pLength, pNulls);
  if (!returnValue)
    return 0;
  
  return 1;
}

int parse_text_1_p_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			char ***parameters, int **parameter_length,
			int *pLength, bool **pNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a approx_entropy_type, 
   * extract the array "list" containing the values to process
   * the parameter arrays "aggtype" for the function
   * and the array "dates" containing the corresponding timestampes
   * to list
   *
   * With the contents of each of these, list will be converted to 
   * double for processing and both arrays will be sorted in increasing
   * time series order.
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);

  if (!returnValue)
    return 0;
    
  /* Get "aggtype" */
  returnValue = get_text_array(t, "parameter", parameters, parameter_length,
			       pLength, pNulls);
  if (!returnValue)
    return 0;
  
  return 1;
}

int parse_list_1_p_type(HeapTupleHeader *t, double **list,
			int *listLength, bool **listNulls,
			int **param, int *paramLength,
			bool **paramNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a value_date_type, 
   * extract the array "list" containing the values to process
   * the array "param" containing the parameters for the function
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array(t, "list",
				 list, listLength, listNulls);
  if (!returnValue)
    return 0;

  /* Get the postgres array for "parameter" */
  returnValue = get_int_array(t, "parameter",
			      param, paramLength, paramNulls);

  if (!returnValue)
    return 0;

  return 1;
}

int parse_list_date_1_p_type(HeapTupleHeader *t, double **list,
			     int *listLength, bool **listNulls,
			     Datum **dates, int *dateLength,
			     bool **dateNulls,
			     int **param, int *paramLength,
			     bool **paramNulls) {
  /*********************************************************************
   * Helper function given the HeapTupleHeader containing all data 
   * passed to a function from postgres as a value_date_type, 
   * extract the array "list" containing the values to process
   * the array "param" containing the parameters for the function
   ********************************************************************/
  int returnValue;

  /* Get the postgres array for "list" */
  returnValue = get_double_array_date(t, list, listLength, listNulls,
				      dates, dateLength, dateNulls);
  if (!returnValue)
    return 0;

  /* Get the postgres array for "parameter" */
  returnValue = get_int_array(t, "parameter",
			      param, paramLength, paramNulls);

  if (!returnValue)
    return 0;

  return 1;
}

void get_digits(int *integral, int *fractional, double value) {
  double ifloor, fround, decimal;
  int pten;

  ifloor = floor(value);
  decimal = value - ifloor;
  pten = 1;
  for(*integral=0; *integral<10; (*integral)++) {
    if (ifloor < pten) break;
    pten *= 10;
  }

  pten = 1;
  for(*fractional=0; *fractional<10; (*fractional)++) {
    fround = round(pten*decimal);
    if (fabs(pten*decimal - fround) < 1e-5) break;
    pten *= 10;
  }
  
  return;
}

void create_fmtstr(char **fmtstr, double value) {
  int integral, fractional, length, negative, ret;

  negative = 0;
  if (fabs(value) != value)
    negative = 1;
  get_digits(&integral, &fractional, fabs(value));
  //printf("%d\t%d\t", integral, fractional);

  //length = 1+integral+fractional+negative;
  //printf("%d\n", length);
  if ((integral == 0) && (value <= 9) && (value >= -9)) {
    integral = 1;
  }
  (*fmtstr) = (char *)palloc(32*sizeof(char));
  if (integral > 0) {
    if (fractional > 0)
      ret = snprintf(*fmtstr, 32, "%%%d.%df", integral+negative+fractional, fractional);
    else
      ret = snprintf(*fmtstr, 32, "%%%d.0f", integral+negative);
  } else {
    if (fractional > 0)
      ret = snprintf(*fmtstr, 32, "%%.%df", fractional);
    else
      *fmtstr = "%f";
  }
  return;
}

