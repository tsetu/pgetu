#include <postgres.h>
#include <fmgr.h>
#include <executor/executor.h>
#include <common/jsonapi.h>
#include <utils/jsonb.h>
#include <etu.h>
#include "tsutils.h"

PG_FUNCTION_INFO_V1(ts_fft_aggregated_calc);

Datum
ts_fft_aggregated_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: fft_aggregated
  Input Type: fft_aggregated_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, dateLength, aLength;
  bool *arrayNulls, *dateNulls, *aNulls;
  double *array;
  Datum *dates;
  char **atypes;
  int *atype_lengths;
  int parse_res, c;

  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_ts_fft_agg_type(&t, &array, &arrayLength, &arrayNulls,
				    &dates, &dateLength, &dateNulls,
				    &atypes, &atype_lengths,
				    &aLength, &aNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(aLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[aLength];
  FFT_AGG params[aLength];

  for (c=0; c<aLength; c++) {
    strncpy(params[c].aggtype, atypes[c], NAME_LENGTH);
  }

  fft_aggregated(array, arrayLength, params, aLength, results);
  
  for (c=0; c<aLength; c++) {
    char *key, *res, *fmtstr;
    int key_len, digits;
    
    key_len = 9+atype_lengths[c];
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    int ret = snprintf(key, key_len, "aggtype_%s", atypes[c]);
    pairs[c].key.type = jbvString;
    pairs[c].key.val.string.len = key_len;
    pairs[c].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[c].value);
    ret = snprintf(res, 32, fmtstr, results[c].value);

    pairs[c].value.type = jbvString;
    pairs[c].value.val.string.len = 32;
    pairs[c].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = aLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}


PG_FUNCTION_INFO_V1(fft_aggregated_calc);

Datum
fft_aggregated_calc(PG_FUNCTION_ARGS) {
  /**************************************************************************
  Function: fft_aggregated
  Input Type: fft_aggregated_type
  Return Type: feature_return
  ***************************************************************************

  **************************************************************************/
  HeapTupleHeader t = PG_GETARG_HEAPTUPLEHEADER(0);
  int m, arrayLength, aLength;
  bool *arrayNulls, *aNulls;
  double *array;
  char **atypes;
  int *atype_lengths;
  int parse_res, c;

  //JsonbParseState *state = NULL;
  JsonbValue *res;
  JsonbPair *pairs;

  /* Parse the value_date_type for array and dates, return sorted by date */
  parse_res = parse_fft_agg_type(&t, &array, &arrayLength, &arrayNulls,
				 &atypes, &atype_lengths,
				 &aLength, &aNulls);

  if (!parse_res) {
    PG_RETURN_NULL();
  }

  /* Create JSONB object to return */
  res = (JsonbValue *)palloc(sizeof(JsonbValue));
  pairs = (JsonbPair *)palloc(aLength*sizeof(JsonbPair));

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  ETU_RETURN results[aLength];
  FFT_AGG params[aLength];

  for (c=0; c<aLength; c++) {
    strncpy(params[c].aggtype, atypes[c], NAME_LENGTH);
  }

  fft_aggregated(array, arrayLength, params, aLength, results);
  
  for (c=0; c<aLength; c++) {
    char *key, *res, *fmtstr;
    int key_len, digits;
    
    key_len = 9+atype_lengths[c];
    key = (char *)palloc(key_len*sizeof(char));
    
    /* Create the key */
    int ret = snprintf(key, key_len, "aggtype_%s", atypes[c]);
    pairs[c].key.type = jbvString;
    pairs[c].key.val.string.len = key_len;
    pairs[c].key.val.string.val = key;

    /* Create the value */
    res = (char *)palloc(32*sizeof(char));
    create_fmtstr(&fmtstr, results[c].value);
    ret = snprintf(res, 32, fmtstr, results[c].value);

    pairs[c].value.type = jbvString;
    pairs[c].value.val.string.len = 32;
    pairs[c].value.val.string.val = res;
  }

  /* Create final Jsonb object to return */
  res->type = jbvObject;
  res->val.object.nPairs = aLength;
  res->val.object.pairs = pairs;
  
  PG_RETURN_POINTER(JsonbValueToJsonb(res));
}
